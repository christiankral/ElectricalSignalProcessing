package PLL
  package Examples
  model LogicImplementation
      PLL.LogicPLL logicPLL(Fmaxe = 1.5e8, Fmine = 5e7, PFDGaine = 1, RtBthresholde = 0.5, Sgaine = 1, VCOoffe = 0, Vmaxe = 2, Vmine = 0, ae = {1, 2.222e6, 0}, be = {3.03e4, 6.122e9}) annotation(
        Placement(visible = true, transformation(origin = {0, 0}, extent = {{-14, -14}, {14, 14}}, rotation = 0)));
  Modelica.Blocks.Sources.IntegerConstant integerConstant(k = 100)  annotation(
        Placement(visible = true, transformation(origin = {-70, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Sources.BooleanPulse booleanPulse(period = 1 / 1e6, startTime = 1 / 2e6, width = 50)  annotation(
        Placement(visible = true, transformation(origin = {-50, -50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      equation
      connect(booleanPulse.y, logicPLL.Clock) annotation(
        Line(points = {{-38, -50}, {-20, -50}, {-20, -6}, {-10, -6}, {-10, -6}}, color = {255, 0, 255}));
      connect(integerConstant.y, logicPLL.DividingFactor) annotation(
        Line(points = {{-58, 0}, {-10, 0}, {-10, 0}, {-10, 0}}, color = {255, 127, 0}));
      connect(logicPLL.y, logicPLL.u) annotation(
        Line(points = {{11, 0}, {41, 0}, {41, 30}, {-39, 30}, {-39, 6}, {-11, 6}}, color = {0, 0, 127}));
    annotation(
      Diagram(coordinateSystem(extent = {{-140, -140}, {140, 140}})),
      Icon(coordinateSystem(extent = {{-140, -140}, {140, 140}})),
      __OpenModelica_commandLineOptions = "",
      __OpenModelica_simulationFlags(lv = "LOG_STATS", noEventEmit = "()", s = "dassl"),
      experiment(StartTime = 0, StopTime = 0.003, Tolerance = 1e-06, Interval = 2e-10)); 
  end LogicImplementation;




  end Examples;
  package Tools
    model JK
      Modelica.Blocks.Interfaces.BooleanInput J annotation(
        Placement(visible = true, transformation(origin = {-100, 50}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, 60}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
      Modelica.Blocks.Interfaces.BooleanInput K annotation(
        Placement(visible = true, transformation(origin = {-100, -50}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, -60}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
      Modelica.Blocks.Interfaces.BooleanOutput Q annotation(
        Placement(visible = true, transformation(origin = {90, 50}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Interfaces.BooleanOutput QI annotation(
        Placement(visible = true, transformation(origin = {90, -50}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, -40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Logical.Pre pre1 annotation(
        Placement(visible = true, transformation(origin = {-50, 70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Logical.Pre pre2 annotation(
        Placement(visible = true, transformation(origin = {-50, -70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Logical.And and1 annotation(
        Placement(visible = true, transformation(origin = {-10, 50}, extent = {{-10, 10}, {10, -10}}, rotation = 0)));
      Modelica.Blocks.Logical.And and2 annotation(
        Placement(visible = true, transformation(origin = {-10, -50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Logical.RSFlipFlop rSFlipFlop annotation(
        Placement(visible = true, transformation(origin = {30, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    equation
      connect(J, and1.u1) annotation(
        Line(points = {{-100, 50}, {-24, 50}, {-24, 50}, {-22, 50}}, color = {255, 0, 255}));
      connect(rSFlipFlop.Q, Q) annotation(
        Line(points = {{42, 6}, {60, 6}, {60, 50}, {90, 50}, {90, 50}}, color = {255, 0, 255}));
      connect(pre2.y, and2.u2) annotation(
        Line(points = {{-38, -70}, {-30, -70}, {-30, -58}, {-22, -58}, {-22, -58}}, color = {255, 0, 255}));
      connect(rSFlipFlop.Q, pre2.u) annotation(
        Line(points = {{42, 6}, {60, 6}, {60, -90}, {-70, -90}, {-70, -70}, {-62, -70}, {-62, -70}}, color = {255, 0, 255}));
      connect(pre1.y, and1.u2) annotation(
        Line(points = {{-38, 70}, {-30, 70}, {-30, 58}, {-22, 58}, {-22, 58}}, color = {255, 0, 255}));
      connect(rSFlipFlop.QI, pre1.u) annotation(
        Line(points = {{42, -6}, {50, -6}, {50, 90}, {-70, 90}, {-70, 70}, {-62, 70}, {-62, 70}}, color = {255, 0, 255}));
      connect(rSFlipFlop.QI, QI) annotation(
        Line(points = {{42, -6}, {50, -6}, {50, -50}, {90, -50}, {90, -50}}, color = {255, 0, 255}));
      connect(and1.y, rSFlipFlop.S) annotation(
        Line(points = {{2, 50}, {10, 50}, {10, 6}, {18, 6}, {18, 6}}, color = {255, 0, 255}));
      connect(and2.y, rSFlipFlop.R) annotation(
        Line(points = {{2, -50}, {10, -50}, {10, -6}, {18, -6}, {18, -6}}, color = {255, 0, 255}));
      connect(K, and2.u1) annotation(
        Line(points = {{-100, -50}, {-22, -50}, {-22, -50}, {-22, -50}}, color = {255, 0, 255}));
    end JK;

    model T
      Modelica.Blocks.Interfaces.BooleanInput T annotation(
        Placement(visible = true, transformation(origin = {-100, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
      Modelica.Blocks.Interfaces.BooleanOutput Q annotation(
        Placement(visible = true, transformation(origin = {90, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Logical.Pre pre1 annotation(
        Placement(visible = true, transformation(origin = {-50, 70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Logical.Pre pre2 annotation(
        Placement(visible = true, transformation(origin = {-50, -70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Logical.And and1 annotation(
        Placement(visible = true, transformation(origin = {-10, 50}, extent = {{-10, 10}, {10, -10}}, rotation = 0)));
      Modelica.Blocks.Logical.And and2 annotation(
        Placement(visible = true, transformation(origin = {-10, -50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Logical.RSFlipFlop rSFlipFlop annotation(
        Placement(visible = true, transformation(origin = {30, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    equation
      connect(T, and2.u1) annotation(
        Line(points = {{-100, 0}, {-40, 0}, {-40, -50}, {-22, -50}, {-22, -50}}, color = {255, 0, 255}));
      connect(T, and1.u1) annotation(
        Line(points = {{-100, 0}, {-40, 0}, {-40, 50}, {-22, 50}, {-22, 50}}, color = {255, 0, 255}));
      connect(rSFlipFlop.Q, Q) annotation(
        Line(points = {{42, 6}, {60, 6}, {60, 0}, {90, 0}}, color = {255, 0, 255}));
      connect(pre2.y, and2.u2) annotation(
        Line(points = {{-38, -70}, {-30, -70}, {-30, -58}, {-22, -58}, {-22, -58}}, color = {255, 0, 255}));
      connect(rSFlipFlop.Q, pre2.u) annotation(
        Line(points = {{42, 6}, {60, 6}, {60, -90}, {-70, -90}, {-70, -70}, {-62, -70}, {-62, -70}}, color = {255, 0, 255}));
      connect(pre1.y, and1.u2) annotation(
        Line(points = {{-38, 70}, {-30, 70}, {-30, 58}, {-22, 58}, {-22, 58}}, color = {255, 0, 255}));
      connect(rSFlipFlop.QI, pre1.u) annotation(
        Line(points = {{42, -6}, {50, -6}, {50, 90}, {-70, 90}, {-70, 70}, {-62, 70}, {-62, 70}}, color = {255, 0, 255}));
      connect(and1.y, rSFlipFlop.S) annotation(
        Line(points = {{2, 50}, {10, 50}, {10, 6}, {18, 6}, {18, 6}}, color = {255, 0, 255}));
      connect(and2.y, rSFlipFlop.R) annotation(
        Line(points = {{2, -50}, {10, -50}, {10, -6}, {18, -6}, {18, -6}}, color = {255, 0, 255}));
    end T;
  
    model Scaler
      extends Modelica.Blocks.Interfaces.SISO;
      parameter Real Sgain "use 1 for direct Proportionality  and -1 for inverse Proportionality ";
      parameter Real Vmax;
      parameter Real Vmin;
      parameter Real Fmax;
      parameter Real Fmin;
    equation
      y = ((Sgain + 1) * (u * Sgain - Vmin) - (Sgain - 1) * (Vmax - u)) / 2 / (Vmax - Vmin) * (Fmax - Fmin) + Fmin;
      annotation(
        Icon(coordinateSystem(extent = {{-100, -100}, {100, 100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2, 2})),
        Diagram(coordinateSystem(extent = {{-100, -100}, {100, 100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2, 2})));
    end Scaler;


    model Pi_integrator
      import Modelica.Blocks.Types.Init;
      parameter Modelica.Blocks.Types.Init initType = Modelica.Blocks.Types.Init.InitialState;
      parameter Real k = 1;
      parameter Real y_start = 0;
      extends Modelica.Blocks.Interfaces.SISO(y(start = y_start));
    initial equation
      if initType == Init.SteadyState then
        der(y) = 0;
      elseif initType == Init.InitialState or initType == Init.InitialOutput then
        y = y_start;
      end if;
    equation
      der(y) = k * u;
      when y > 1 then
        reinit(y, 0);
      end when;
    end Pi_integrator;
  end Tools;


  package Real_io_components
    model Count
      Modelica.Blocks.Interfaces.BooleanInput u1 annotation(
        Placement(visible = true, transformation(origin = {-100, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
      Modelica.Blocks.Interfaces.IntegerInput u2 annotation(
        Placement(visible = true, transformation(origin = {-10, -60}, extent = {{-20, -20}, {20, 20}}, rotation = 90), iconTransformation(origin = {0, -120}, extent = {{-20, -20}, {20, 20}}, rotation = 90)));
      Modelica.Blocks.Interfaces.BooleanOutput y annotation(
        Placement(visible = true, transformation(origin = {90, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Logical.Change change annotation(
        Placement(visible = true, transformation(origin = {-60, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.MathInteger.TriggeredAdd triggeredAdd(use_reset = true, y_start = 0) annotation(
        Placement(visible = true, transformation(origin = {-50, 70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Sources.IntegerConstant integerConstant(k = 1) annotation(
        Placement(visible = true, transformation(origin = {-90, 70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Sources.Constant const(k = 0.5) annotation(
        Placement(visible = true, transformation(origin = {30, -10}, extent = {{10, -10}, {-10, 10}}, rotation = -90)));
      Modelica.Blocks.Logical.Pre pre annotation(
        Placement(visible = true, transformation(origin = {10, -80}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
      Modelica.Blocks.Math.Add add(k1 = +1, k2 = -1) annotation(
        Placement(visible = true, transformation(origin = {10, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
      Modelica.Blocks.Logical.Greater greater annotation(
        Placement(visible = true, transformation(origin = {30, 70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Math.IntegerToReal integerToReal1 annotation(
        Placement(visible = true, transformation(origin = {-10, 70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Math.IntegerToReal integerToReal2 annotation(
        Placement(visible = true, transformation(origin = {-10, -10}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
  PLL.Tools.T T annotation(
        Placement(visible = true, transformation(origin = {70, 70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    equation
      connect(T.Q, y) annotation(
        Line(points = {{82, 70}, {90, 70}, {90, 40}, {70, 40}, {70, 0}, {90, 0}, {90, 0}}, color = {255, 0, 255}));
      connect(T.T, greater.y) annotation(
        Line(points = {{58, 70}, {40, 70}, {40, 70}, {42, 70}}, color = {255, 0, 255}));
      connect(integerConstant.y, triggeredAdd.u) annotation(
        Line(points = {{-78, 70}, {-64, 70}, {-64, 70}, {-64, 70}}, color = {255, 127, 0}));
      connect(triggeredAdd.y, integerToReal1.u) annotation(
        Line(points = {{-38, 70}, {-22, 70}, {-22, 70}, {-22, 70}}, color = {255, 127, 0}));
      connect(integerToReal1.y, greater.u1) annotation(
        Line(points = {{2, 70}, {18, 70}, {18, 70}, {18, 70}}, color = {0, 0, 127}));
      connect(add.y, greater.u2) annotation(
        Line(points = {{10, 42}, {10, 42}, {10, 62}, {18, 62}, {18, 62}}, color = {0, 0, 127}));
      connect(pre.u, greater.y) annotation(
        Line(points = {{22, -80}, {50, -80}, {50, 70}, {42, 70}, {42, 70}}, color = {255, 0, 255}));
      connect(pre.y, triggeredAdd.reset) annotation(
        Line(points = {{-2, -80}, {-30, -80}, {-30, 40}, {-44, 40}, {-44, 58}, {-44, 58}}, color = {255, 0, 255}));
      connect(change.y, triggeredAdd.trigger) annotation(
        Line(points = {{-48, 0}, {-40, 0}, {-40, 20}, {-56, 20}, {-56, 58}, {-56, 58}}, color = {255, 0, 255}));
      connect(u1, change.u) annotation(
        Line(points = {{-100, 0}, {-72, 0}, {-72, 0}, {-72, 0}}, color = {255, 0, 255}));
      connect(integerToReal2.y, add.u1) annotation(
        Line(points = {{-10, 2}, {-10, 2}, {-10, 10}, {4, 10}, {4, 18}, {4, 18}}, color = {0, 0, 127}));
      connect(integerToReal2.u, u2) annotation(
        Line(points = {{-10, -22}, {-10, -22}, {-10, -60}, {-10, -60}}, color = {255, 127, 0}));
      connect(const.y, add.u2) annotation(
        Line(points = {{30, 2}, {30, 2}, {30, 10}, {16, 10}, {16, 18}, {16, 18}}, color = {0, 0, 127}));
    end Count;

    model LVCO
      parameter Real VCOoff "offset of the output signal";
      extends Modelica.Blocks.Interfaces.SISO;
      Modelica.Blocks.Continuous.Integrator integrator annotation(
        Placement(visible = true, transformation(origin = {-70, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Math.Gain gain(k = 4 * Modelica.Math.asin(1.0)) annotation(
        Placement(visible = true, transformation(origin = {-30, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Math.Sin sin annotation(
        Placement(visible = true, transformation(origin = {10, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Math.Add add annotation(
        Placement(visible = true, transformation(origin = {50, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Sources.Constant const(k = VCOoff)  annotation(
        Placement(visible = true, transformation(origin = {10, -50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    equation
      connect(add.y, y) annotation(
        Line(points = {{62, 0}, {104, 0}, {104, 0}, {110, 0}}, color = {0, 0, 127}));
      connect(const.y, add.u2) annotation(
        Line(points = {{22, -50}, {30, -50}, {30, -6}, {38, -6}, {38, -6}}, color = {0, 0, 127}));
      connect(add.u1, sin.y) annotation(
        Line(points = {{38, 6}, {30, 6}, {30, 0}, {22, 0}, {22, 0}}, color = {0, 0, 127}));
      connect(sin.u, gain.y) annotation(
        Line(points = {{-2, 0}, {-19, 0}}, color = {0, 0, 127}));
      connect(gain.u, integrator.y) annotation(
        Line(points = {{-42, 0}, {-59, 0}}, color = {0, 0, 127}));
      connect(integrator.u, u) annotation(
        Line(points = {{-82, 0}, {-120, 0}}, color = {0, 0, 127}));
    end LVCO;




    model LVCO2
      extends Modelica.Blocks.Interfaces.SISO;
      PLL.Tools.Pi_integrator pi_integrator annotation(
        Placement(visible = true, transformation(origin = {-70, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Math.Gain gain(k = 4 * Modelica.Math.asin(1.0))  annotation(
        Placement(visible = true, transformation(origin = {-30, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Math.Sin sin annotation(
        Placement(visible = true, transformation(origin = {10, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Sources.Constant const annotation(
        Placement(visible = true, transformation(origin = {10, -50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Math.Add add1 annotation(
        Placement(visible = true, transformation(origin = {60, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    equation
      connect(add1.y, y) annotation(
        Line(points = {{72, 0}, {106, 0}, {106, 0}, {110, 0}}, color = {0, 0, 127}));
      connect(sin.y, add1.u1) annotation(
        Line(points = {{22, 0}, {30, 0}, {30, 6}, {48, 6}, {48, 6}}, color = {0, 0, 127}));
      connect(const.y, add1.u2) annotation(
        Line(points = {{22, -50}, {40, -50}, {40, -6}, {48, -6}, {48, -6}}, color = {0, 0, 127}));
      connect(gain.y, sin.u) annotation(
        Line(points = {{-19, 0}, {-2, 0}}, color = {0, 0, 127}));
      connect(pi_integrator.y, gain.u) annotation(
        Line(points = {{-59, 0}, {-42, 0}}, color = {0, 0, 127}));
      connect(pi_integrator.u, u) annotation(
        Line(points = {{-82, 0}, {-120, 0}}, color = {0, 0, 127}));
    end LVCO2;
  end Real_io_components;

  package Hybrid_io_components
    model PFD
      parameter Real gain = 1;
      Modelica.Blocks.Interfaces.BooleanInput u1 annotation(
        Placement(visible = true, transformation(origin = {-100, 70}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, 60}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
      Modelica.Blocks.Interfaces.BooleanInput u2 annotation(
        Placement(visible = true, transformation(origin = {-100, -70}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, -60}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
      Modelica.Blocks.Logical.Edge edge1 annotation(
        Placement(visible = true, transformation(origin = {-50, 70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Logical.Edge edge2 annotation(
        Placement(visible = true, transformation(origin = {-50, -70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Logical.Pre pre annotation(
        Placement(visible = true, transformation(origin = {-50, 0}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
      Modelica.Blocks.Logical.And and1 annotation(
        Placement(visible = true, transformation(origin = {-10, 0}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
      Modelica.Blocks.Logical.RSFlipFlop rSFlipFlop1 annotation(
        Placement(visible = true, transformation(origin = {-10, 50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Logical.RSFlipFlop rSFlipFlop2 annotation(
        Placement(visible = true, transformation(origin = {-10, -50}, extent = {{-10, 10}, {10, -10}}, rotation = 0)));
      Modelica.Blocks.Math.BooleanToReal booleanToReal1 annotation(
        Placement(visible = true, transformation(origin = {50, 70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Math.BooleanToReal booleanToReal2 annotation(
        Placement(visible = true, transformation(origin = {50, -70}, extent = {{-10, 10}, {10, -10}}, rotation = 0)));
      Modelica.Blocks.Math.Add add(k1 = -gain, k2 = +gain) annotation(
        Placement(visible = true, transformation(origin = {50, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Interfaces.RealOutput y annotation(
        Placement(visible = true, transformation(origin = {90, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    equation
      connect(and1.y, pre.u) annotation(
        Line(points = {{-20, 0}, {-38, 0}, {-38, 0}, {-38, 0}}, color = {255, 0, 255}));
      connect(rSFlipFlop1.Q, and1.u1) annotation(
        Line(points = {{2, 56}, {20, 56}, {20, 0}, {2, 0}, {2, 0}}, color = {255, 0, 255}));
      connect(rSFlipFlop2.Q, and1.u2) annotation(
        Line(points = {{2, -56}, {20, -56}, {20, -8}, {2, -8}, {2, -8}}, color = {255, 0, 255}));
      connect(booleanToReal1.y, add.u1) annotation(
        Line(points = {{62, 70}, {70, 70}, {70, 40}, {30, 40}, {30, 6}, {38, 6}, {38, 6}}, color = {0, 0, 127}));
      connect(rSFlipFlop1.Q, booleanToReal1.u) annotation(
        Line(points = {{2, 56}, {20, 56}, {20, 70}, {38, 70}, {38, 70}}, color = {255, 0, 255}));
      connect(add.y, y) annotation(
        Line(points = {{62, 0}, {88, 0}, {88, 0}, {90, 0}}, color = {0, 0, 127}));
      connect(booleanToReal2.y, add.u2) annotation(
        Line(points = {{62, -70}, {70, -70}, {70, -40}, {30, -40}, {30, -6}, {38, -6}, {38, -6}}, color = {0, 0, 127}));
      connect(rSFlipFlop2.Q, booleanToReal2.u) annotation(
        Line(points = {{2, -56}, {20, -56}, {20, -70}, {38, -70}, {38, -70}}, color = {255, 0, 255}));
      connect(edge1.y, rSFlipFlop1.S) annotation(
        Line(points = {{-38, 70}, {-30, 70}, {-30, 56}, {-22, 56}, {-22, 56}}, color = {255, 0, 255}));
      connect(u1, edge1.u) annotation(
        Line(points = {{-100, 70}, {-62, 70}, {-62, 70}, {-62, 70}}, color = {255, 0, 255}));
      connect(pre.y, rSFlipFlop1.R) annotation(
        Line(points = {{-62, 0}, {-80, 0}, {-80, 44}, {-22, 44}, {-22, 44}}, color = {255, 0, 255}));
      connect(pre.y, rSFlipFlop2.R) annotation(
        Line(points = {{-62, 0}, {-80, 0}, {-80, -44}, {-22, -44}, {-22, -44}}, color = {255, 0, 255}));
      connect(edge2.y, rSFlipFlop2.S) annotation(
        Line(points = {{-38, -70}, {-30, -70}, {-30, -56}, {-22, -56}, {-22, -56}}, color = {255, 0, 255}));
      connect(u2, edge2.u) annotation(
        Line(points = {{-100, -70}, {-62, -70}, {-62, -70}, {-62, -70}}, color = {255, 0, 255}));
    end PFD;

    model VtoB
      Modelica.Electrical.Analog.Interfaces.PositivePin pin_p annotation(
        Placement(visible = true, transformation(origin = {-90, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Interfaces.NegativePin pin_n annotation(
        Placement(visible = true, transformation(origin = {0, -90}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {0, -110}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Sensors.VoltageSensor voltageSensor annotation(
        Placement(visible = true, transformation(origin = {0, -30}, extent = {{-10, 10}, {10, -10}}, rotation = -90)));
      Modelica.Blocks.Math.RealToBoolean realToBoolean(threshold = 0) annotation(
        Placement(visible = true, transformation(origin = {50, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Interfaces.BooleanOutput y annotation(
        Placement(visible = true, transformation(origin = {90, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    equation
      connect(voltageSensor.p, pin_p) annotation(
        Line(points = {{0, -20}, {0, -20}, {0, 0}, {-90, 0}, {-90, 0}}, color = {0, 0, 255}));
      connect(pin_n, voltageSensor.n) annotation(
        Line(points = {{0, -90}, {0, -90}, {0, -40}, {0, -40}}, color = {0, 0, 255}));
      connect(y, realToBoolean.y) annotation(
        Line(points = {{90, 0}, {62, 0}, {62, 0}, {62, 0}}, color = {255, 0, 255}));
      connect(realToBoolean.u, voltageSensor.v) annotation(
        Line(points = {{38, 0}, {20, 0}, {20, -30}, {10, -30}, {10, -30}}, color = {0, 0, 127}));
    end VtoB;

    model VCO
      Modelica.Blocks.Interfaces.RealInput u annotation(
        Placement(visible = true, transformation(origin = {-100, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
      Modelica.Blocks.Continuous.Integrator integrator annotation(
        Placement(visible = true, transformation(origin = {-50, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Math.Gain gain(k = 4 * Modelica.Math.asin(1.0)) annotation(
        Placement(visible = true, transformation(origin = {0, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Math.Sin sin annotation(
        Placement(visible = true, transformation(origin = {50, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Interfaces.Pin pin annotation(
        Placement(visible = true, transformation(origin = {90, 50}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Interfaces.NegativePin pin_n annotation(
        Placement(visible = true, transformation(origin = {90, -50}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {0, -110}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Sources.SignalVoltage signalVoltage annotation(
        Placement(visible = true, transformation(origin = {90, 0}, extent = {{10, -10}, {-10, 10}}, rotation = 90)));
    equation
      connect(signalVoltage.p, pin) annotation(
        Line(points = {{90, 10}, {90, 10}, {90, 50}, {90, 50}}, color = {0, 0, 255}));
      connect(signalVoltage.n, pin_n) annotation(
        Line(points = {{90, -10}, {90, -10}, {90, -50}, {90, -50}}, color = {0, 0, 255}));
      connect(integrator.u, u) annotation(
        Line(points = {{-62, 0}, {-92, 0}, {-92, 0}, {-100, 0}}, color = {0, 0, 127}));
      connect(gain.u, integrator.y) annotation(
        Line(points = {{-12, 0}, {-40, 0}, {-40, 0}, {-38, 0}}, color = {0, 0, 127}));
      connect(sin.u, gain.y) annotation(
        Line(points = {{38, 0}, {10, 0}, {10, 0}, {12, 0}}, color = {0, 0, 127}));
      connect(signalVoltage.v, sin.y) annotation(
        Line(points = {{82, 0}, {60, 0}, {60, 0}, {62, 0}}, color = {0, 0, 127}));
    end VCO;

    model VCO2
      Modelica.Blocks.Interfaces.RealInput u annotation(
        Placement(visible = true, transformation(origin = {-100, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
      Modelica.Blocks.Math.Gain gain(k = 4 * Modelica.Math.asin(1.0)) annotation(
        Placement(visible = true, transformation(origin = {0, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Math.Sin sin annotation(
        Placement(visible = true, transformation(origin = {50, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Interfaces.Pin pin annotation(
        Placement(visible = true, transformation(origin = {90, 50}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Interfaces.NegativePin pin_n annotation(
        Placement(visible = true, transformation(origin = {90, -50}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {0, -110}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Sources.SignalVoltage signalVoltage annotation(
        Placement(visible = true, transformation(origin = {90, 0}, extent = {{10, -10}, {-10, 10}}, rotation = 90)));
      PLL.Tools.Pi_integrator pi_integrator annotation(
        Placement(visible = true, transformation(origin = {-50, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    equation
      connect(pi_integrator.y, gain.u) annotation(
        Line(points = {{-38, 0}, {-12, 0}, {-12, 0}, {-12, 0}}, color = {0, 0, 127}));
      connect(u, pi_integrator.u) annotation(
        Line(points = {{-100, 0}, {-64, 0}, {-64, 0}, {-62, 0}}, color = {0, 0, 127}));
      connect(signalVoltage.p, pin) annotation(
        Line(points = {{90, 10}, {90, 10}, {90, 50}, {90, 50}}, color = {0, 0, 255}));
      connect(signalVoltage.n, pin_n) annotation(
        Line(points = {{90, -10}, {90, -10}, {90, -50}, {90, -50}}, color = {0, 0, 255}));
      connect(sin.u, gain.y) annotation(
        Line(points = {{38, 0}, {10, 0}, {10, 0}, {12, 0}}, color = {0, 0, 127}));
      connect(signalVoltage.v, sin.y) annotation(
        Line(points = {{82, 0}, {60, 0}, {60, 0}, {62, 0}}, color = {0, 0, 127}));
    end VCO2;
  end Hybrid_io_components;

  package Loop_filters
    model Second_order_passive
      parameter Real Ik;
      parameter Real R2;
      parameter Real C1;
      parameter Real C2;
      parameter Real C1v;
      parameter Real C2v;
      Modelica.Blocks.Interfaces.RealInput u annotation(
        Placement(visible = true, transformation(origin = {-100, -30}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
      Modelica.Electrical.Analog.Interfaces.Pin pin annotation(
        Placement(visible = true, transformation(origin = {90, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Interfaces.NegativePin pin_n annotation(
        Placement(visible = true, transformation(origin = {0, -90}, extent = {{-10, 10}, {10, -10}}, rotation = 180), iconTransformation(origin = {0, -110}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Sources.SignalCurrent signalCurrent annotation(
        Placement(visible = true, transformation(origin = {-20, -30}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
      Modelica.Electrical.Analog.Basic.Resistor resistor2(R = R2) annotation(
        Placement(visible = true, transformation(origin = {40, -50}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
      Modelica.Electrical.Analog.Basic.Capacitor capacitor2(C = C2, v(fixed = true, start = C1v)) annotation(
        Placement(visible = true, transformation(origin = {40, -30}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
      Modelica.Electrical.Analog.Basic.Capacitor capacitor1(C = C1, v(fixed = true, start = C1v)) annotation(
        Placement(visible = true, transformation(origin = {10, -30}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
      Modelica.Blocks.Math.Gain gain(k = Ik) annotation(
        Placement(visible = true, transformation(origin = {-50, -30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    equation
      connect(signalCurrent.n, capacitor1.p) annotation(
        Line(points = {{-20, -20}, {-20, 0}, {10, 0}, {10, -20}}, color = {0, 0, 255}));
      connect(capacitor1.n, pin_n) annotation(
        Line(points = {{10, -40}, {10, -90}, {0, -90}}, color = {0, 0, 255}));
      connect(gain.y, signalCurrent.i) annotation(
        Line(points = {{-38, -30}, {-28, -30}, {-28, -30}, {-26, -30}}, color = {0, 0, 127}));
      connect(u, gain.u) annotation(
        Line(points = {{-100, -30}, {-62, -30}, {-62, -30}, {-62, -30}}, color = {0, 0, 127}));
      connect(signalCurrent.p, pin_n) annotation(
        Line(points = {{-20, -40}, {-20, -40}, {-20, -90}, {0, -90}, {0, -90}}, color = {0, 0, 255}));
      connect(resistor2.n, pin_n) annotation(
        Line(points = {{40, -60}, {40, -60}, {40, -90}, {0, -90}, {0, -90}}, color = {0, 0, 255}));
      connect(capacitor2.p, signalCurrent.n) annotation(
        Line(points = {{40, -20}, {40, 0}, {-20, 0}, {-20, -20}}, color = {0, 0, 255}));
      connect(resistor2.p, capacitor2.n) annotation(
        Line(points = {{40, -40}, {40, -40}}, color = {0, 0, 255}));
      connect(pin, capacitor2.p) annotation(
        Line(points = {{90, 0}, {40, 0}, {40, -20}}, color = {0, 0, 255}));
    end Second_order_passive;

    model Third_order_passive
      parameter Real Ik;
      parameter Real R2;
      parameter Real R3;
      parameter Real C1;
      parameter Real C2;
      parameter Real C3;
      parameter Real C1v;
      parameter Real C2v;
      parameter Real C3v;
      Modelica.Blocks.Interfaces.RealInput u annotation(
        Placement(visible = true, transformation(origin = {-100, -30}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
      Modelica.Electrical.Analog.Interfaces.Pin pin annotation(
        Placement(visible = true, transformation(origin = {90, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Interfaces.NegativePin pin_n annotation(
        Placement(visible = true, transformation(origin = {0, -90}, extent = {{-10, 10}, {10, -10}}, rotation = 180), iconTransformation(origin = {0, -110}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Sources.SignalCurrent signalCurrent annotation(
        Placement(visible = true, transformation(origin = {-30, -30}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
      Modelica.Electrical.Analog.Basic.Resistor resistor2(R = R2) annotation(
        Placement(visible = true, transformation(origin = {30, -50}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
      Modelica.Electrical.Analog.Basic.Capacitor capacitor2(C = C2, v(fixed = true, start = C2v)) annotation(
        Placement(visible = true, transformation(origin = {30, -30}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
      Modelica.Electrical.Analog.Basic.Capacitor capacitor1(C = C1, v(fixed = true, start = C1v)) annotation(
        Placement(visible = true, transformation(origin = {0, -30}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
      Modelica.Electrical.Analog.Basic.Capacitor capacitor3(C = C3, v(start = C3v)) annotation(
        Placement(visible = true, transformation(origin = {70, -50}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
      Modelica.Electrical.Analog.Basic.Resistor resistor3(R = R3) annotation(
        Placement(visible = true, transformation(origin = {50, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Math.Gain gain(k = Ik) annotation(
        Placement(visible = true, transformation(origin = {-60, -30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    equation
      connect(u, gain.u) annotation(
        Line(points = {{-100, -30}, {-74, -30}, {-74, -30}, {-72, -30}}, color = {0, 0, 127}));
      connect(gain.y, signalCurrent.i) annotation(
        Line(points = {{-48, -30}, {-36, -30}, {-36, -30}, {-36, -30}}, color = {0, 0, 127}));
      connect(capacitor3.n, pin_n) annotation(
        Line(points = {{70, -60}, {70, -60}, {70, -90}, {0, -90}, {0, -90}}, color = {0, 0, 255}));
      connect(resistor2.n, pin_n) annotation(
        Line(points = {{30, -60}, {30, -60}, {30, -90}, {0, -90}, {0, -90}}, color = {0, 0, 255}));
      connect(capacitor1.n, pin_n) annotation(
        Line(points = {{0, -40}, {0, -40}, {0, -90}, {0, -90}}, color = {0, 0, 255}));
      connect(signalCurrent.p, pin_n) annotation(
        Line(points = {{-30, -40}, {-30, -40}, {-30, -90}, {0, -90}, {0, -90}}, color = {0, 0, 255}));
      connect(resistor3.p, capacitor2.p) annotation(
        Line(points = {{40, 0}, {36, 0}, {36, 0}, {30, 0}, {30, -20}, {31, -20}, {31, -20}, {30, -20}}, color = {0, 0, 255}));
      connect(capacitor3.p, resistor3.n) annotation(
        Line(points = {{70, -40}, {70, -40}, {70, 0}, {60, 0}, {60, 0}}, color = {0, 0, 255}));
      connect(resistor3.n, pin) annotation(
        Line(points = {{60, 0}, {90, 0}}, color = {0, 0, 255}));
      connect(signalCurrent.n, capacitor1.p) annotation(
        Line(points = {{-30, -20}, {-30, -20}, {-30, 0}, {0, 0}, {0, -20}, {0, -20}, {0, -20}, {0, -20}}, color = {0, 0, 255}));
      connect(resistor2.p, capacitor2.n) annotation(
        Line(points = {{30, -40}, {30, -40}}, color = {0, 0, 255}));
      connect(capacitor2.p, signalCurrent.n) annotation(
        Line(points = {{30, -20}, {30, -20}, {30, 0}, {-30, 0}, {-30, -20}, {-30, -20}, {-30, -20}, {-30, -20}}, color = {0, 0, 255}));
    end Third_order_passive;

    model Fourth_order_passive
      parameter Real Ik;
      parameter Real R2;
      parameter Real R3;
      parameter Real R4;
      parameter Real C1;
      parameter Real C2;
      parameter Real C3;
      parameter Real C4;
      parameter Real C1v;
      parameter Real C2v;
      parameter Real C3v;
      parameter Real C4v;
      Modelica.Blocks.Interfaces.RealInput u annotation(
        Placement(visible = true, transformation(origin = {-80, 40}, extent = {{-20, -20}, {20, 20}}, rotation = -90), iconTransformation(origin = {-120, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
      Modelica.Electrical.Analog.Interfaces.Pin pin annotation(
        Placement(visible = true, transformation(origin = {90, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Interfaces.NegativePin pin_n annotation(
        Placement(visible = true, transformation(origin = {0, -90}, extent = {{-10, 10}, {10, -10}}, rotation = 180), iconTransformation(origin = {0, -110}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Sources.SignalCurrent signalCurrent annotation(
        Placement(visible = true, transformation(origin = {-60, -30}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
      Modelica.Electrical.Analog.Basic.Resistor resistor2(R = R2) annotation(
        Placement(visible = true, transformation(origin = {0, -50}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
      Modelica.Electrical.Analog.Basic.Capacitor capacitor2(C = C2, v(fixed = true, start = C2v)) annotation(
        Placement(visible = true, transformation(origin = {0, -30}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
      Modelica.Electrical.Analog.Basic.Capacitor capacitor1(C = C1, v(fixed = true, start = C1v)) annotation(
        Placement(visible = true, transformation(origin = {-30, -30}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
      Modelica.Electrical.Analog.Basic.Capacitor capacitor3(C = C3, v(fixed = true, start = C3v)) annotation(
        Placement(visible = true, transformation(origin = {40, -50}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
      Modelica.Electrical.Analog.Basic.Resistor resistor3(R = R3) annotation(
        Placement(visible = true, transformation(origin = {20, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Basic.Resistor resistor1(R = R4) annotation(
        Placement(visible = true, transformation(origin = {60, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Basic.Capacitor capacitor4(C = C4, v(fixed = true, start = C4v)) annotation(
        Placement(visible = true, transformation(origin = {80, -50}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
      Modelica.Blocks.Math.Gain gain(k = Ik) annotation(
        Placement(visible = true, transformation(origin = {-80, -10}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
    equation
      connect(signalCurrent.i, gain.y) annotation(
        Line(points = {{-66, -30}, {-80, -30}, {-80, -21}}, color = {0, 0, 127}));
      connect(u, gain.u) annotation(
        Line(points = {{-80, 40}, {-80, 2}}, color = {0, 0, 127}));
      connect(signalCurrent.n, capacitor1.p) annotation(
        Line(points = {{-60, -20}, {-60, 0}, {-30, 0}, {-30, -20}}, color = {0, 0, 255}));
      connect(capacitor2.p, signalCurrent.n) annotation(
        Line(points = {{0, -20}, {0, 0}, {-60, 0}, {-60, -20}}, color = {0, 0, 255}));
      connect(signalCurrent.p, pin_n) annotation(
        Line(points = {{-60, -40}, {-60, -90}, {0, -90}}, color = {0, 0, 255}));
      connect(resistor1.p, resistor3.n) annotation(
        Line(points = {{50, 0}, {30, 0}, {30, 0}, {30, 0}}, color = {0, 0, 255}));
      connect(resistor1.n, pin) annotation(
        Line(points = {{70, 0}, {88, 0}, {88, 0}, {90, 0}}, color = {0, 0, 255}));
      connect(capacitor4.p, resistor1.n) annotation(
        Line(points = {{80, -40}, {80, -40}, {80, 0}, {70, 0}, {70, 0}}, color = {0, 0, 255}));
      connect(capacitor4.n, pin_n) annotation(
        Line(points = {{80, -60}, {80, -60}, {80, -90}, {0, -90}, {0, -90}}, color = {0, 0, 255}));
      connect(capacitor3.n, pin_n) annotation(
        Line(points = {{40, -60}, {40, -60}, {40, -90}, {0, -90}, {0, -90}}, color = {0, 0, 255}));
      connect(capacitor3.p, resistor3.n) annotation(
        Line(points = {{40, -40}, {40, -40}, {40, 0}, {30, 0}, {30, 0}}, color = {0, 0, 255}));
      connect(resistor3.p, capacitor2.p) annotation(
        Line(points = {{10, 0}, {0, 0}, {0, -20}, {0, -20}}, color = {0, 0, 255}));
      connect(capacitor1.n, pin_n) annotation(
        Line(points = {{-30, -40}, {-30, -40}, {-30, -90}, {0, -90}, {0, -90}}, color = {0, 0, 255}));
      connect(resistor2.n, pin_n) annotation(
        Line(points = {{0, -60}, {0, -60}, {0, -90}, {0, -90}}, color = {0, 0, 255}));
      connect(resistor2.p, capacitor2.n) annotation(
        Line(points = {{0, -40}, {0, -40}}, color = {0, 0, 255}));
    end Fourth_order_passive;

    model Second_order_active
      parameter Real R1;
      parameter Real R2;
      parameter Real C1;
      parameter Real C2;
      parameter Real C1v;
      parameter Real C2v;
      Modelica.Electrical.Analog.Ideal.IdealOpAmpLimited idealOpAmpLimited1 annotation(
        Placement(visible = true, transformation(origin = {0, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Interfaces.NegativePin pin_n annotation(
        Placement(visible = true, transformation(origin = {0, -30}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {40, -110}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Interfaces.NegativePin pin_ref annotation(
        Placement(visible = true, transformation(origin = {0, -90}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-40, -110}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Basic.Resistor resistor1(R = R1) annotation(
        Placement(visible = true, transformation(origin = {-60, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Basic.Resistor resistor2(R = R2) annotation(
        Placement(visible = true, transformation(origin = {-20, 60}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Interfaces.PositivePin pin_p annotation(
        Placement(visible = true, transformation(origin = {0, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {0, 110}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Interfaces.RealInput u annotation(
        Placement(visible = true, transformation(origin = {-40, -30}, extent = {{20, -20}, {-20, 20}}, rotation = 0), iconTransformation(origin = {-120, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
      Modelica.Electrical.Analog.Sources.SignalVoltage signalVoltage annotation(
        Placement(visible = true, transformation(origin = {-80, -30}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
      Modelica.Electrical.Analog.Basic.Capacitor capacitor1(C = C1, v(fixed = true, start = C1v)) annotation(
        Placement(visible = true, transformation(origin = {0, 80}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Basic.Capacitor capacitor2(C = C2, v(fixed = true, start = C2v)) annotation(
        Placement(visible = true, transformation(origin = {20, 60}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Interfaces.Pin pin annotation(
        Placement(visible = true, transformation(origin = {90, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    equation
      connect(idealOpAmpLimited1.in_n, capacitor1.p) annotation(
        Line(points = {{-10, 6}, {-40, 6}, {-40, 80}, {-10, 80}, {-10, 80}}, color = {0, 0, 255}));
      connect(capacitor1.n, idealOpAmpLimited1.out) annotation(
        Line(points = {{10, 80}, {40, 80}, {40, 0}, {10, 0}, {10, 0}}, color = {0, 0, 255}));
      connect(capacitor2.n, idealOpAmpLimited1.out) annotation(
        Line(points = {{30, 60}, {40, 60}, {40, 0}, {10, 0}, {10, 0}}, color = {0, 0, 255}));
      connect(resistor2.p, idealOpAmpLimited1.in_n) annotation(
        Line(points = {{-30, 60}, {-40, 60}, {-40, 6}, {-10, 6}, {-10, 6}}, color = {0, 0, 255}));
      connect(resistor2.n, capacitor2.p) annotation(
        Line(points = {{-10, 60}, {10, 60}, {10, 60}, {10, 60}}, color = {0, 0, 255}));
      connect(idealOpAmpLimited1.out, pin) annotation(
        Line(points = {{10, 0}, {90, 0}, {90, 0}, {90, 0}}, color = {0, 0, 255}));
      connect(idealOpAmpLimited1.in_p, pin_ref) annotation(
        Line(points = {{-10, -4}, {-20, -4}, {-20, -90}, {0, -90}, {0, -90}}, color = {0, 0, 255}));
      connect(pin_n, idealOpAmpLimited1.VMin) annotation(
        Line(points = {{0, -30}, {0, -7}}, color = {0, 0, 255}));
      connect(resistor1.n, idealOpAmpLimited1.in_n) annotation(
        Line(points = {{-50, 0}, {-40, 0}, {-40, 5}, {-10, 5}}, color = {0, 0, 255}));
      connect(pin_p, idealOpAmpLimited1.VMax) annotation(
        Line(points = {{0, 30}, {0, 7}}, color = {0, 0, 255}));
      connect(signalVoltage.p, resistor1.p) annotation(
        Line(points = {{-80, -20}, {-80, -20}, {-80, 0}, {-70, 0}, {-70, 0}}, color = {0, 0, 255}));
      connect(u, signalVoltage.v) annotation(
        Line(points = {{-40, -30}, {-72, -30}, {-72, -30}, {-72, -30}}, color = {0, 0, 127}));
      connect(pin_ref, signalVoltage.n) annotation(
        Line(points = {{0, -90}, {-80, -90}, {-80, -40}, {-80, -40}}, color = {0, 0, 255}));
    end Second_order_active;

    model Third_order_active
      parameter Real R1;
      parameter Real R2;
      parameter Real R3;
      parameter Real C1;
      parameter Real C2;
      parameter Real C3;
      parameter Real C1v;
      parameter Real C2v;
      parameter Real C3v;
      Modelica.Electrical.Analog.Ideal.IdealOpAmpLimited idealOpAmpLimited1 annotation(
        Placement(visible = true, transformation(origin = {0, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Interfaces.NegativePin pin_n annotation(
        Placement(visible = true, transformation(origin = {0, -30}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {40, -110}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Interfaces.NegativePin pin_ref annotation(
        Placement(visible = true, transformation(origin = {0, -90}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-40, -110}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Basic.Resistor resistor2(R = R2) annotation(
        Placement(visible = true, transformation(origin = {-20, 60}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Interfaces.PositivePin pin_p annotation(
        Placement(visible = true, transformation(origin = {0, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {0, 110}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Interfaces.RealInput u annotation(
        Placement(visible = true, transformation(origin = {-40, -30}, extent = {{20, -20}, {-20, 20}}, rotation = 0), iconTransformation(origin = {-120, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
      Modelica.Electrical.Analog.Basic.Capacitor capacitor1(C = C1, v(fixed = true, start = C1v)) annotation(
        Placement(visible = true, transformation(origin = {0, 80}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Basic.Capacitor capacitor2(C = C2, v(fixed = true, start = C2v)) annotation(
        Placement(visible = true, transformation(origin = {20, 60}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Interfaces.Pin pin annotation(
        Placement(visible = true, transformation(origin = {90, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Basic.Resistor resistor1(R = R1) annotation(
        Placement(visible = true, transformation(origin = {-60, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Sources.SignalVoltage signalVoltage annotation(
        Placement(visible = true, transformation(origin = {-80, -30}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
      Modelica.Electrical.Analog.Basic.Resistor resistor3(R = R3) annotation(
        Placement(visible = true, transformation(origin = {30, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Basic.Capacitor capacitor3(C = C3, v(fixed = true, start = C3v)) annotation(
        Placement(visible = true, transformation(origin = {50, -30}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
    equation
      connect(idealOpAmpLimited1.out, capacitor1.n) annotation(
        Line(points = {{10, 0}, {14, 0}, {14, 40}, {40, 40}, {40, 80}, {10, 80}, {10, 80}}, color = {0, 0, 255}));
      connect(idealOpAmpLimited1.out, capacitor2.n) annotation(
        Line(points = {{10, 0}, {14, 0}, {14, 20}, {14, 20}, {14, 40}, {40, 40}, {40, 60}, {30, 60}, {30, 60}}, color = {0, 0, 255}));
      connect(idealOpAmpLimited1.in_n, capacitor1.p) annotation(
        Line(points = {{-10, 6}, {-40, 6}, {-40, 80}, {-10, 80}, {-10, 80}}, color = {0, 0, 255}));
      connect(idealOpAmpLimited1.in_n, resistor2.p) annotation(
        Line(points = {{-10, 6}, {-40, 6}, {-40, 60}, {-30, 60}, {-30, 60}}, color = {0, 0, 255}));
      connect(resistor2.n, capacitor2.p) annotation(
        Line(points = {{-10, 60}, {10, 60}, {10, 60}, {10, 60}}, color = {0, 0, 255}));
      connect(resistor3.n, pin) annotation(
        Line(points = {{40, 0}, {88, 0}, {88, 0}, {90, 0}}, color = {0, 0, 255}));
      connect(pin_p, idealOpAmpLimited1.VMax) annotation(
        Line(points = {{0, 30}, {0, 7}}, color = {0, 0, 255}));
      connect(idealOpAmpLimited1.out, resistor3.p) annotation(
        Line(points = {{10, 0}, {20, 0}, {20, 0}, {20, 0}}, color = {0, 0, 255}));
      connect(capacitor3.p, resistor3.n) annotation(
        Line(points = {{50, -20}, {50, -20}, {50, 0}, {40, 0}, {40, 0}, {40, 0}}, color = {0, 0, 255}));
      connect(capacitor3.n, pin_ref) annotation(
        Line(points = {{50, -40}, {50, -40}, {50, -90}, {0, -90}, {0, -90}}, color = {0, 0, 255}));
      connect(signalVoltage.p, resistor1.p) annotation(
        Line(points = {{-80, -20}, {-80, -20}, {-80, 0}, {-70, 0}, {-70, 0}}, color = {0, 0, 255}));
      connect(u, signalVoltage.v) annotation(
        Line(points = {{-40, -30}, {-72, -30}, {-72, -30}, {-72, -30}}, color = {0, 0, 127}));
      connect(pin_ref, signalVoltage.n) annotation(
        Line(points = {{0, -90}, {-80, -90}, {-80, -40}, {-80, -40}}, color = {0, 0, 255}));
      connect(resistor1.n, idealOpAmpLimited1.in_n) annotation(
        Line(points = {{-50, 0}, {-40, 0}, {-40, 5}, {-10, 5}}, color = {0, 0, 255}));
      connect(idealOpAmpLimited1.in_p, pin_ref) annotation(
        Line(points = {{-10, -4}, {-20, -4}, {-20, -90}, {0, -90}, {0, -90}}, color = {0, 0, 255}));
      connect(pin_n, idealOpAmpLimited1.VMin) annotation(
        Line(points = {{0, -30}, {0, -7}}, color = {0, 0, 255}));
    end Third_order_active;

    model Fourth_order_active
      parameter Real R1;
      parameter Real R2;
      parameter Real R3;
      parameter Real R4;
      parameter Real C1;
      parameter Real C2;
      parameter Real C3;
      parameter Real C4;
      parameter Real C1v;
      parameter Real C2v;
      parameter Real C3v;
      parameter Real C4v;
      Modelica.Electrical.Analog.Ideal.IdealOpAmpLimited idealOpAmpLimited1 annotation(
        Placement(visible = true, transformation(origin = {0, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Interfaces.NegativePin pin_n annotation(
        Placement(visible = true, transformation(origin = {0, -30}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {40, -110}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Interfaces.NegativePin pin_ref annotation(
        Placement(visible = true, transformation(origin = {0, -90}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-40, -110}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Basic.Resistor resistor2(R = R2) annotation(
        Placement(visible = true, transformation(origin = {-20, 60}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Interfaces.PositivePin pin_p annotation(
        Placement(visible = true, transformation(origin = {0, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {0, 110}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Blocks.Interfaces.RealInput u annotation(
        Placement(visible = true, transformation(origin = {-40, -30}, extent = {{20, -20}, {-20, 20}}, rotation = 0), iconTransformation(origin = {-120, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
      Modelica.Electrical.Analog.Basic.Capacitor capacitor1(C = C1, v(fixed = true, start = C1v)) annotation(
        Placement(visible = true, transformation(origin = {0, 80}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Basic.Capacitor capacitor2(C = C2, v(fixed = true, start = C2v)) annotation(
        Placement(visible = true, transformation(origin = {20, 60}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Interfaces.Pin pin annotation(
        Placement(visible = true, transformation(origin = {90, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Basic.Resistor resistor1(R = R1) annotation(
        Placement(visible = true, transformation(origin = {-60, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Sources.SignalVoltage signalVoltage annotation(
        Placement(visible = true, transformation(origin = {-80, -30}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
      Modelica.Electrical.Analog.Basic.Resistor resistor3(R = R3) annotation(
        Placement(visible = true, transformation(origin = {30, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Basic.Capacitor capacitor3(C = C3, v(fixed = true, start = C3v)) annotation(
        Placement(visible = true, transformation(origin = {40, -30}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
      Modelica.Electrical.Analog.Basic.Resistor resistor4(R = R4) annotation(
        Placement(visible = true, transformation(origin = {60, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Basic.Capacitor capacitor4(C = C4, v(fixed = true, start = C4v)) annotation(
        Placement(visible = true, transformation(origin = {80, -30}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
    equation
      connect(capacitor1.n, idealOpAmpLimited1.out) annotation(
        Line(points = {{10, 80}, {40, 80}, {40, 40}, {14, 40}, {14, 0}, {10, 0}, {10, 0}}, color = {0, 0, 255}));
      connect(idealOpAmpLimited1.in_n, capacitor1.p) annotation(
        Line(points = {{-10, 6}, {-40, 6}, {-40, 80}, {-10, 80}, {-10, 80}}, color = {0, 0, 255}));
      connect(idealOpAmpLimited1.in_n, resistor2.p) annotation(
        Line(points = {{-10, 6}, {-40, 6}, {-40, 60}, {-30, 60}, {-30, 60}}, color = {0, 0, 255}));
      connect(idealOpAmpLimited1.out, capacitor2.n) annotation(
        Line(points = {{10, 0}, {14, 0}, {14, 40}, {40, 40}, {40, 60}, {30, 60}, {30, 60}}, color = {0, 0, 255}));
      connect(resistor2.n, capacitor2.p) annotation(
        Line(points = {{-10, 60}, {10, 60}, {10, 60}, {10, 60}}, color = {0, 0, 255}));
      connect(pin, resistor4.n) annotation(
        Line(points = {{90, 0}, {70, 0}, {70, 0}, {70, 0}}, color = {0, 0, 255}));
      connect(capacitor4.p, resistor4.n) annotation(
        Line(points = {{80, -20}, {80, -20}, {80, 0}, {70, 0}, {70, 0}}, color = {0, 0, 255}));
      connect(resistor4.p, resistor3.n) annotation(
        Line(points = {{50, 0}, {40, 0}, {40, 0}, {40, 0}}, color = {0, 0, 255}));
      connect(capacitor4.n, pin_ref) annotation(
        Line(points = {{80, -40}, {80, -40}, {80, -90}, {0, -90}, {0, -90}}, color = {0, 0, 255}));
      connect(capacitor3.n, pin_ref) annotation(
        Line(points = {{40, -40}, {40, -90}, {0, -90}}, color = {0, 0, 255}));
      connect(capacitor3.p, resistor3.n) annotation(
        Line(points = {{40, -20}, {40, 0}}, color = {0, 0, 255}));
      connect(pin_p, idealOpAmpLimited1.VMax) annotation(
        Line(points = {{0, 30}, {0, 7}}, color = {0, 0, 255}));
      connect(idealOpAmpLimited1.out, resistor3.p) annotation(
        Line(points = {{10, 0}, {20, 0}, {20, 0}, {20, 0}}, color = {0, 0, 255}));
      connect(signalVoltage.p, resistor1.p) annotation(
        Line(points = {{-80, -20}, {-80, -20}, {-80, 0}, {-70, 0}, {-70, 0}}, color = {0, 0, 255}));
      connect(u, signalVoltage.v) annotation(
        Line(points = {{-40, -30}, {-72, -30}, {-72, -30}, {-72, -30}}, color = {0, 0, 127}));
      connect(pin_ref, signalVoltage.n) annotation(
        Line(points = {{0, -90}, {-80, -90}, {-80, -40}, {-80, -40}}, color = {0, 0, 255}));
      connect(resistor1.n, idealOpAmpLimited1.in_n) annotation(
        Line(points = {{-50, 0}, {-40, 0}, {-40, 5}, {-10, 5}}, color = {0, 0, 255}));
      connect(idealOpAmpLimited1.in_p, pin_ref) annotation(
        Line(points = {{-10, -4}, {-20, -4}, {-20, -90}, {0, -90}, {0, -90}}, color = {0, 0, 255}));
      connect(pin_n, idealOpAmpLimited1.VMin) annotation(
        Line(points = {{0, -30}, {0, -7}}, color = {0, 0, 255}));
    end Fourth_order_active;
  end Loop_filters;

  model LogicPLL
    parameter Real RtBthresholde "Threshold for swiching the boolean value into the frequency divider";
    parameter Real PFDGaine "Gain of the Phase Frequency Detector";
    parameter Real be[:]={1} "Numerator coefficients of transfer function (e.g., 2*s+3 is specified as {2,3})";
    parameter Real ae[:]={1} "Denominator coefficients of transfer function (e.g., 5*s+6 is specified as {5,6})";
    parameter Real Sgaine "use 1 for direct Proportionality  and -1 for inverse Proportionality ";
    parameter Real Vmaxe "Maximum Value in the simulated Voltage Controlled Oscillator input";
    parameter Real Vmine "Minumum Value in the simulated Voltage Controlled Oscillator input";
    parameter Real Fmaxe "Maximum Frequency in the simulated Voltage Controlled Oscillator output";
    parameter Real Fmine "Minumum Frequency in the simulated Voltage Controlled Oscillator output";
    parameter Real VCOoffe "Offset for periodic signal from VCO";
    Modelica.Blocks.Math.RealToBoolean RealToBoolean(threshold = RtBthresholde)  annotation(
      Placement(visible = true, transformation(origin = {-90, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    PLL.Real_io_components.Count count annotation(
      Placement(visible = true, transformation(origin = {-50, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    PLL.Hybrid_io_components.PFD pfd(gain = PFDGaine) annotation(
      Placement(visible = true, transformation(origin = {-10, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Modelica.Blocks.Continuous.TransferFunction transferFunction(a = be, b = be, initType = Modelica.Blocks.Types.Init.InitialState) annotation(
      Placement(visible = true, transformation(origin = {30, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    PLL.Tools.Scaler scaler( Fmax = Fmaxe, Fmin = Fmine, Sgain = Sgaine, Vmax = Vmaxe, Vmin = Vmine) annotation(
      Placement(visible = true, transformation(origin = {70, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    PLL.Real_io_components.LVCO lvco(VCOoff = VCOoffe)  annotation(
      Placement(visible = true, transformation(origin = {110, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Interfaces.IntegerInput DividingFactor annotation(
      Placement(visible = true, transformation(origin = {-50, -30}, extent = {{-10, -10}, {10, 10}}, rotation = 90), iconTransformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Interfaces.BooleanInput Clock annotation(
      Placement(visible = true, transformation(origin = {-10, -30}, extent = {{-10, -10}, {10, 10}}, rotation = 90), iconTransformation(origin = {-110, -60}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Interfaces.RealOutput y annotation(
      Placement(visible = true, transformation(origin = {150, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Interfaces.RealInput u annotation(
      Placement(visible = true, transformation(origin = {-130, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, 60}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Interfaces.RealOutput Freq annotation(
      Placement(visible = true, transformation(origin = {150, 70}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  equation
    connect(scaler.y, lvco.u) annotation(
      Line(points = {{82, 30}, {98, 30}}, color = {0, 0, 127}));
    connect(y, lvco.y) annotation(
      Line(points = {{150, 30}, {121, 30}}, color = {0, 0, 127}));
    connect(scaler.y, Freq) annotation(
      Line(points = {{82, 30}, {90, 30}, {90, 70}, {150, 70}, {150, 70}}, color = {0, 0, 127}));
    connect(RealToBoolean.u, u) annotation(
      Line(points = {{-102, 30}, {-126, 30}, {-126, 30}, {-130, 30}}, color = {0, 0, 127}));
    connect(pfd.u2, Clock) annotation(
      Line(points = {{-22, 24}, {-30, 24}, {-30, 0}, {-10, 0}, {-10, -30}, {-10, -30}}, color = {255, 0, 255}));
    connect(count.u2, DividingFactor) annotation(
      Line(points = {{-50, 18}, {-50, 18}, {-50, -30}, {-50, -30}}, color = {255, 127, 0}));
    connect(transferFunction.y, scaler.u) annotation(
      Line(points = {{42, 30}, {56, 30}, {56, 30}, {58, 30}}, color = {0, 0, 127}));
    connect(pfd.y, transferFunction.u) annotation(
      Line(points = {{2, 30}, {16, 30}, {16, 30}, {18, 30}}, color = {0, 0, 127}));
    connect(count.y, pfd.u1) annotation(
      Line(points = {{-38, 30}, {-30, 30}, {-30, 36}, {-22, 36}, {-22, 36}}, color = {255, 0, 255}));
    connect(RealToBoolean.y, count.u1) annotation(
      Line(points = {{-78, 30}, {-64, 30}, {-64, 30}, {-62, 30}}, color = {255, 0, 255}));
    annotation(
      Diagram(coordinateSystem(extent = {{-140, -140}, {140, 140}})),
      Icon(coordinateSystem(extent = {{-140, -140}, {140, 140}})),
      __OpenModelica_commandLineOptions = "",
      __OpenModelica_simulationFlags(lv = "LOG_STATS", noEventEmit = "()", s = "dassl"),
      experiment(StartTime = 0, StopTime = 0.003, Tolerance = 1e-06, Interval = 2e-10));
  end LogicPLL;





















  model OpampPLL
    Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage(V = 2) annotation(
      Placement(visible = true, transformation(origin = {-140, 0}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
    PLL.Hybrid_io_components.VtoB vtoB annotation(
      Placement(visible = true, transformation(origin = {-100, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    PLL.Real_io_components.Count count annotation(
      Placement(visible = true, transformation(origin = {-60, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    PLL.Hybrid_io_components.PFD pfd(gain = 1) annotation(
      Placement(visible = true, transformation(origin = {-20, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Modelica.Electrical.Analog.Sensors.VoltageSensor voltageSensor annotation(
      Placement(visible = true, transformation(origin = {50, 10}, extent = {{-10, 10}, {10, -10}}, rotation = -90)));
    Hybrid_io_components.VCO vco annotation(
      Placement(visible = true, transformation(origin = {130, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage1(V = 1) annotation(
      Placement(visible = true, transformation(origin = {10, -10}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
    Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage2(V = 0) annotation(
      Placement(visible = true, transformation(origin = {30, -10}, extent = {{10, -10}, {-10, 10}}, rotation = -90)));
    Modelica.Electrical.Analog.Basic.Ground ground annotation(
      Placement(visible = true, transformation(origin = {0, -50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Tools.Scaler scaler(Fmax = 15e7, Fmin = 5e7, Vmax = 2, Vmin = 0, k = -1) annotation(
      Placement(visible = true, transformation(origin = {90, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Modelica.Blocks.Sources.IntegerConstant integerConstant(k = 125) annotation(
      Placement(visible = true, transformation(origin = {-60, -10}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
    Modelica.Blocks.Sources.BooleanPulse booleanPulse1(period = 1 / 1e6, startTime = 0.25 / 1e6, width = 50) annotation(
      Placement(visible = true, transformation(origin = {-30, -10}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
    PLL.Loop_filters.Second_order_active second_order_active(C1 = 3.3e-8, C1v = 0, C2 = 3.3e-7, C2v = 0, R1 = 1e3, R2 = 15) annotation(
      Placement(visible = true, transformation(origin = {20, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  equation
    connect(second_order_active.pin_p, constantVoltage.p) annotation(
      Line(points = {{20, 42}, {20, 42}, {20, 70}, {-140, 70}, {-140, 10}, {-140, 10}}, color = {0, 0, 255}));
    connect(second_order_active.pin, voltageSensor.p) annotation(
      Line(points = {{32, 30}, {50, 30}, {50, 20}, {50, 20}, {50, 20}}, color = {0, 0, 255}));
    connect(constantVoltage2.n, second_order_active.pin_n) annotation(
      Line(points = {{30, 0}, {30, 0}, {30, 10}, {24, 10}, {24, 20}, {24, 20}}, color = {0, 0, 255}));
    connect(second_order_active.u, pfd.y) annotation(
      Line(points = {{8, 30}, {-8, 30}}, color = {0, 0, 127}));
    connect(second_order_active.pin_ref, constantVoltage1.p) annotation(
      Line(points = {{16, 19}, {16, 9.5}, {10, 9.5}, {10, 0}}, color = {0, 0, 255}));
    connect(constantVoltage.n, ground.p) annotation(
      Line(points = {{-140, -10}, {-140, -30}, {0, -30}, {0, -40}}, color = {0, 0, 255}));
    connect(vco.pin, vtoB.pin_p) annotation(
      Line(points = {{142, 30}, {150, 30}, {150, 60}, {-120, 60}, {-120, 30}, {-110, 30}, {-110, 30}}, color = {0, 0, 255}));
    connect(booleanPulse1.y, pfd.u2) annotation(
      Line(points = {{-30, 2}, {-30, 2}, {-30, 10}, {-40, 10}, {-40, 24}, {-32, 24}, {-32, 24}}, color = {255, 0, 255}));
    connect(count.u2, integerConstant.y) annotation(
      Line(points = {{-60, 18}, {-60, 18}, {-60, 2}, {-60, 2}}, color = {255, 127, 0}));
    connect(vtoB.y, count.u1) annotation(
      Line(points = {{-88, 30}, {-74, 30}, {-74, 30}, {-72, 30}}, color = {255, 0, 255}));
    connect(count.y, pfd.u1) annotation(
      Line(points = {{-48, 30}, {-40, 30}, {-40, 36}, {-32, 36}, {-32, 36}}, color = {255, 0, 255}));
    connect(vtoB.pin_n, ground.p) annotation(
      Line(points = {{-100, 20}, {-100, 20}, {-100, -30}, {0, -30}, {0, -40}, {0, -40}}, color = {0, 0, 255}));
    connect(voltageSensor.n, ground.p) annotation(
      Line(points = {{50, 0}, {50, 0}, {50, -30}, {0, -30}, {0, -40}, {0, -40}}, color = {0, 0, 255}));
    connect(constantVoltage1.n, ground.p) annotation(
      Line(points = {{10, -20}, {10, -20}, {10, -30}, {0, -30}, {0, -40}, {0, -40}}, color = {0, 0, 255}));
    connect(constantVoltage2.p, ground.p) annotation(
      Line(points = {{30, -20}, {30, -20}, {30, -30}, {0, -30}, {0, -40}, {0, -40}}, color = {0, 0, 255}));
    connect(vco.pin_n, ground.p) annotation(
      Line(points = {{130, 20}, {130, 20}, {130, -30}, {0, -30}, {0, -40}, {0, -40}}, color = {0, 0, 255}));
    connect(scaler.y, vco.u) annotation(
      Line(points = {{102, 30}, {116, 30}, {116, 30}, {118, 30}}, color = {0, 0, 127}));
    connect(voltageSensor.v, scaler.u) annotation(
      Line(points = {{60, 10}, {70, 10}, {70, 30}, {78, 30}, {78, 30}}, color = {0, 0, 127}));
    annotation(
      Diagram(coordinateSystem(extent = {{-160, -80}, {160, 80}})),
      Icon(coordinateSystem(extent = {{-160, -80}, {160, 80}})),
      __OpenModelica_commandLineOptions = "",
      experiment(StartTime = 0, StopTime = 0.003, Tolerance = 1e-09, Interval = 2e-10));
  end OpampPLL;

  model PassivePLL
    PLL.Hybrid_io_components.VtoB vtoB annotation(
      Placement(visible = true, transformation(origin = {-110, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    PLL.Real_io_components.Count count annotation(
      Placement(visible = true, transformation(origin = {-70, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    PLL.Hybrid_io_components.PFD pfd1(gain = 1) annotation(
      Placement(visible = true, transformation(origin = {-30, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Modelica.Electrical.Analog.Sensors.VoltageSensor voltageSensor annotation(
      Placement(visible = true, transformation(origin = {50, 0}, extent = {{-10, 10}, {10, -10}}, rotation = -90)));
    Tools.Scaler scaler(Fmax = 15e7, Fmin = 5e7, Vmax = 2, Vmin = 0, k = 1) annotation(
      Placement(visible = true, transformation(origin = {90, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Hybrid_io_components.VCO vco annotation(
      Placement(visible = true, transformation(origin = {130, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Modelica.Electrical.Analog.Basic.Ground ground1 annotation(
      Placement(visible = true, transformation(origin = {10, -50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    Modelica.Blocks.Sources.BooleanPulse booleanPulse(period = 1 / 1e6, startTime = 0.5 / 1e6, width = 50) annotation(
      Placement(visible = true, transformation(origin = {-30, -10}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
    Modelica.Blocks.Sources.IntegerConstant integerConstant(k = 75) annotation(
      Placement(visible = true, transformation(origin = {-70, -10}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
    PLL.Loop_filters.Second_order_passive second_order_passive(C1 = 3.3e-8, C1v = 0, C2 = 3.3e-7, C2v = 0, Ik = 0.001, R2 = 15) annotation(
      Placement(visible = true, transformation(origin = {10, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  equation
    connect(second_order_passive.u, pfd1.y) annotation(
      Line(points = {{-2, 30}, {-20, 30}, {-20, 30}, {-18, 30}}, color = {0, 0, 127}));
    connect(second_order_passive.pin_n, ground1.p) annotation(
      Line(points = {{10, 20}, {10, 20}, {10, -40}, {10, -40}}, color = {0, 0, 255}));
    connect(second_order_passive.pin, voltageSensor.p) annotation(
      Line(points = {{22, 30}, {50, 30}, {50, 10}, {50, 10}}, color = {0, 0, 255}));
    connect(scaler.u, voltageSensor.v) annotation(
      Line(points = {{78, 30}, {70, 30}, {70, 0}, {60, 0}}, color = {0, 0, 127}));
    connect(ground1.p, voltageSensor.n) annotation(
      Line(points = {{10, -40}, {50, -40}, {50, -10}}, color = {0, 0, 255}));
    connect(vco.pin, vtoB.pin_p) annotation(
      Line(points = {{142, 30}, {150, 30}, {150, 60}, {-130, 60}, {-130, 30}, {-120, 30}, {-120, 30}}, color = {0, 0, 255}));
    connect(count.u2, integerConstant.y) annotation(
      Line(points = {{-70, 18}, {-70, 18}, {-70, 2}, {-70, 2}}, color = {255, 127, 0}));
    connect(pfd1.u2, booleanPulse.y) annotation(
      Line(points = {{-42, 24}, {-50, 24}, {-50, 10}, {-30, 10}, {-30, 2}, {-30, 2}}, color = {255, 0, 255}));
    connect(ground1.p, vtoB.pin_n) annotation(
      Line(points = {{10, -40}, {-110, -40}, {-110, -10}, {-110, -10}, {-110, 20}}, color = {0, 0, 255}));
    connect(ground1.p, vco.pin_n) annotation(
      Line(points = {{10, -40}, {130, -40}, {130, 20}}, color = {0, 0, 255}));
    connect(pfd1.u1, count.y) annotation(
      Line(points = {{-42, 36}, {-50, 36}, {-50, 30}, {-58, 30}, {-58, 30}, {-58, 30}, {-58, 30}}, color = {255, 0, 255}));
    connect(count.u1, vtoB.y) annotation(
      Line(points = {{-82, 30}, {-98, 30}, {-98, 30}, {-98, 30}}, color = {255, 0, 255}));
    connect(scaler.y, vco.u) annotation(
      Line(points = {{102, 30}, {118, 30}, {118, 30}, {118, 30}}, color = {0, 0, 127}));
    annotation(
      Icon(coordinateSystem(extent = {{-140, -80}, {160, 80}})),
      Diagram(coordinateSystem(extent = {{-140, -80}, {160, 80}})),
      __OpenModelica_commandLineOptions = "",
      experiment(StartTime = 0, StopTime = 0.003, Tolerance = 1e-09, Interval = 2e-10));
  end PassivePLL;
  annotation(
    uses(Modelica(version = "3.2.2")));
end PLL;
