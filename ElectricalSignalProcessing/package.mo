within ;
package ElectricalSignalProcessing "Electrical signal processing blocks"
extends Modelica.Icons.Package;

  annotation (version="0.3.X", uses(Modelica(version="4.0.0")), Icon(graphics={
      Line(points={{-60,-40},{-60,40}}, color={127,0,0}),
      Line(points={{-40,-60},{-40,60}}, color={62,130,19}),
      Line(points={{-20,-80},{-20,80}}, color={49,57,174}),
      Line(points={{0,-40},{0,40}}, color={127,0,0}),
      Line(points={{20,-20},{20,20}}, color={62,130,19}),
      Line(points={{40,-60},{40,60}}, color={49,57,174}),
      Line(points={{60,-40},{60,40}}, color={127,0,0})}),
    conversion(from(version="0.2.X", script="modelica://ElectricalSignalProcessing/Resources/ConvertFromElectricalSignalProcessing_0.2.X.mos")));
end ElectricalSignalProcessing;
