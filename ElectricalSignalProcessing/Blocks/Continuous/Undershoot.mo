within ElectricalSignalProcessing.Blocks.Continuous;
block Undershoot
  "Output is set equal to input when input undershoots threshold"
  extends Modelica.Blocks.Interfaces.SISO;
  parameter Real threshold = 0 "Threshold";
algorithm
  when u <= threshold then
    y := time;
  end when;
  when (-u) < threshold then
    y := time;
  end when;
  annotation (Icon(graphics={
        Polygon(
          points={{18,-70},{22,-52},{14,-52},{18,-70}},
          lineColor={255,0,0},
          fillColor={255,0,0},
          fillPattern=FillPattern.Solid),
        Line(points={{18,40},{18,-70}},   color={255,0,0}),
        Line(
          points={{82,-70},{62,-8},{22,40},{-22,62},{-78,70}},
          color={0,0,0},
          smooth=Smooth.Bezier),
        Line(points={{-78,40},{82,40}}, color={255,0,0}),
        Ellipse(
          extent={{12,46},{24,34}},
          lineColor={0,0,0},
          fillColor={255,0,0},
          fillPattern=FillPattern.Solid)}));
end Undershoot;
