within ElectricalSignalProcessing.Blocks.Continuous;
block DetectAngleSpeed "Detect angle and speed of a space phasor"
  extends Modelica.Blocks.Icons.Block;
  extends Modelica.Icons.UnderConstruction;
  final parameter Integer m=3 "Number of phases";
  parameter Real k=1E3 "Proportinal gain for generating angle";
  parameter Modelica.Units.SI.Time Ti=1e-6
    "Integrator time constant for generating angle";
  parameter Modelica.Units.SI.Angle phi0=0 "Initial angle";
  Modelica.Blocks.Continuous.PI PI(
    k=k,
    T=Ti,
    initType=Modelica.Blocks.Types.Init.InitialOutput)
    annotation (Placement(transformation(extent={{-30,-10},{-10,10}})));
  Modelica.Blocks.Interfaces.RealInput wEstimation "Estimated angular velocity"
    annotation (Placement(transformation(
        extent={{-20,-20},{20,20}},
        rotation=90,
        origin={0,-120})));
  Modelica.Blocks.Math.Add addSpeed(k2=1)
    annotation (Placement(transformation(extent={{10,-10},{30,10}})));
  Modelica.Blocks.Interfaces.RealInput u[m] "Phase variables"
    annotation (Placement(transformation(extent={{-140,-20},{-100,20}})));
  Modelica.Blocks.Continuous.Integrator integrator(initType=Modelica.Blocks.Types.Init.InitialOutput, y_start=
        phi0)
    annotation (Placement(transformation(extent={{50,-10},{70,10}})));
  Modelica.Blocks.Interfaces.RealOutput phi "Angle of space phasor"
    annotation (Placement(transformation(extent={{100,-10},{120,10}})));
  Modelica.Blocks.Interfaces.RealOutput wElectrical
    "Angular velocity of space phasor"
    annotation (Placement(transformation(extent={{100,-70},{120,-50}})));
  Modelica.Electrical.Machines.SpacePhasors.Blocks.ToSpacePhasor toSpacePhasor
    annotation (Placement(transformation(extent={{-90,-10},{-70,10}})));
  Modelica.Electrical.Machines.SpacePhasors.Blocks.Rotator rotator
    annotation (Placement(transformation(extent={{-60,-10},{-40,10}})));
equation
  connect(PI.y, addSpeed.u1)
    annotation (Line(points={{-9,0},{0,0},{0,6},{8,6}}, color={0,0,127}));
  connect(wEstimation, addSpeed.u2)
    annotation (Line(points={{0,-120},{0,-6},{8,-6}}, color={0,0,127}));
  connect(addSpeed.y, integrator.u)
    annotation (Line(points={{31,0},{48,0}}, color={0,0,127}));
  connect(integrator.y, phi)
    annotation (Line(points={{71,0},{110,0}}, color={0,0,127}));
  connect(addSpeed.y, wElectrical) annotation (Line(points={{31,0},{40,0},{40,
          -60},{110,-60}},
                      color={0,0,127}));
  connect(u, toSpacePhasor.u)
    annotation (Line(points={{-120,0},{-92,0}}, color={0,0,127}));
  connect(toSpacePhasor.y, rotator.u)
    annotation (Line(points={{-69,0},{-62,0}}, color={0,0,127}));
  connect(rotator.y[2], PI.u)
    annotation (Line(points={{-39,0},{-32,0}}, color={0,0,127}));
  connect(integrator.y, rotator.angle) annotation (Line(points={{71,0},{80,0},{80,
          -20},{-50,-20},{-50,-12}}, color={0,0,127}));
  annotation (Icon(graphics={
        Line(points={{-80,0},{-20,0}},  color={0,140,72}),
        Ellipse(extent={{-10,-60},{10,-80}},  lineColor={95,95,95},
          fillColor={175,175,175},
          fillPattern=FillPattern.Sphere),
        Polygon(points={{-20,0},{-28,4},{-28,-4},{-20,0}}, lineColor={0,140,72},
          fillColor={0,140,72},
          fillPattern=FillPattern.Solid),
        Line(points={{20,0},{80,0}}, color={28,108,200}),
        Polygon(points={{80,0},{72,4},{72,-4},{80,0}}, lineColor={28,108,200},
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-40,-60},{80,60}},
          lineColor={28,108,200},
          startAngle=0,
          endAngle=45),
        Polygon(
          points={{4,0},{-4,4},{-4,-4},{4,0}},
          lineColor={28,108,200},
          origin={66,38},
          rotation=135,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid)}), Documentation(info="<html>
<p>
To detect phase angle and frequency of m=3 phase variables, first the phase variables are transformed to the corresponding spcae phasor.
Then in order to detect angular position and angular velocity of the rotating space phasor, this space phasor is transformed to a rotating coordinate system, 
i.e. rotated by an angle has to be detected by that block. 
A PI controller is used to control imaginary part of the space phasor to zero, i.e. to detect deviation of angular velocity from estimated angular velocity. 
Adding the estimation of the angular velocity, we obtain the desired angular velocity. 
Integrating angular velocity, we obtain the desired angle of the space phasor.
</p>
</html>"));
end DetectAngleSpeed;
