within ElectricalSignalProcessing.Blocks.Continuous;
block SubsequentMax "Determine subsequent maximum values"
  extends Modelica.Blocks.Interfaces.SIMO(final nout = n);
  parameter Integer n = 5 "Number of outputs";
  discrete Real umax[n](start = zeros(n), fixed = fill(true, n)) "Maximum values";
  discrete Modelica.Units.SI.Time t[n](start=zeros(n), fixed=fill(true, n))
    "Time instants of maximum values";
  Integer i "Index";
initial algorithm
  i := 1;
algorithm
  when der(u) <= 0 then
    if i <= n then
      umax[i] := u;
      y[i] := u;
      t[i] := time;
      i := pre(i) + 1;
    end if;
  end when;
  annotation (Icon(graphics={
        Line(
          points={{-78,-16},{-66,44},{-56,56},{-44,34},{-32,-22},{-20,-62},
              {0,-76},{14,-50},{22,-20},{36,38},{54,72},{70,60},{80,26}},
          color={95,95,95},
          smooth=Smooth.Bezier),
        Ellipse(
          extent={{52,74},{64,62}},
          lineColor={0,0,0},
          fillColor={255,0,0},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-62,60},{-50,48}},
          lineColor={0,0,0},
          fillColor={255,0,0},
          fillPattern=FillPattern.Solid)}));
end SubsequentMax;
