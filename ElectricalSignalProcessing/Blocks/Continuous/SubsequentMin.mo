within ElectricalSignalProcessing.Blocks.Continuous;
block SubsequentMin "Determine subsequent minimum values"
  extends Modelica.Blocks.Interfaces.SIMO(final nout = n);
  parameter Integer n = 5 "Number of outputs";
  discrete Real umin[n](start = zeros(n), fixed = fill(true, n)) "Minimum values";
  discrete Modelica.Units.SI.Time t[n](start=zeros(n), fixed=fill(true, n))
    "Time instants of maximum values";
  Integer i "Index";
initial algorithm
  i := 1;
algorithm
  when der(u) >= 0 then
    if i <= n then
      umin[i] := u;
      y[i] := u;
      t[i] := time;
      i := pre(i) + 1;
    end if;
  end when;
  annotation (Icon(graphics={
        Line(
          points={{-78,14},{-66,-46},{-56,-58},{-44,-36},{-32,20},{-20,60},
              {0,74},{14,48},{22,18},{36,-40},{54,-74},{70,-62},{80,-28}},
          color={95,95,95},
          smooth=Smooth.Bezier),
        Ellipse(
          extent={{52,-76},{64,-64}},
          lineColor={0,0,0},
          fillColor={255,0,0},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-62,-60},{-50,-48}},
          lineColor={0,0,0},
          fillColor={255,0,0},
          fillPattern=FillPattern.Solid)}));
end SubsequentMin;
