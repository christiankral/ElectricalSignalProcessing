within ElectricalSignalProcessing.Blocks.Continuous;
block MinMax "Determine minimum and maximum values within given time interval"
  Modelica.Blocks.Interfaces.RealInput u annotation (
    Placement(transformation(extent = {{-140, -20}, {-100, 20}}), iconTransformation(extent = {{-140, -20}, {-100, 20}})));
  Modelica.Blocks.Interfaces.RealOutput yMax annotation (
    Placement(transformation(extent = {{100, 50}, {120, 70}}), iconTransformation(extent = {{100, 50}, {120, 70}})));
  Modelica.Blocks.Interfaces.RealOutput yMin annotation (
    Placement(transformation(extent = {{100, -70}, {120, -50}}), iconTransformation(extent = {{100, -70}, {120, -50}})));
  parameter Modelica.Units.SI.Time startTime = 0 "Start time of evaluation of min/max";
  parameter Modelica.Units.SI.Time stopTime = 1 "Stop time of evaluation of min/max";
  parameter Modelica.Units.SI.Time eps = 1E-6 "Epsilon time";
  discrete Real uMax(fixed = false) "Maximum value";
  discrete Real uMin(fixed = false) "Minimum value";
  discrete Modelica.Units.SI.Time tMax(fixed = false) "Time instant of maximum value";
  discrete Modelica.Units.SI.Time tMin(fixed = false) "Time instant of minimum value";
initial algorithm
  uMax := u;
  uMin := u;
  tMax := time;
  tMin := time;
equation
algorithm
  // Initialization
  when time >= startTime then
    uMax := u;
    uMin := u;
    tMax := time;
    tMin := time;
    end when;
// Look for maximum
  when der(u) <= 0 then
    if time > startTime + eps and time <= stopTime then
      if u > pre(uMax) then
        uMax := u;
        tMax := time;
      else
        uMax := pre(uMax);
      end if;
    end if;
  end when;
  yMax := uMax;
// Look for minimum
  when der(u) >= 0 then
    if time >= startTime + eps and time <= stopTime then
      if u < pre(uMin) then
        uMin := u;
        tMin := time;
      else
        uMin := pre(uMin);
      end if;
    end if;
  end when;
  yMin := uMin;
  annotation (
    Icon(graphics={  Rectangle(lineColor = {0, 0, 127}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, -100}, {100, 100}}), Line(points = {{-78, -16}, {-66, 44}, {-56, 56}, {-44, 34}, {-32, -22}, {-20, -62}, {0, -76}, {14, -50}, {22, -20}, {36, 38}, {54, 72}, {70, 60}, {80, 26}}, color = {95, 95, 95}, smooth = Smooth.Bezier), Ellipse(fillColor = {255, 0, 0}, fillPattern = FillPattern.Solid, extent = {{52, 74}, {64, 62}}), Ellipse(fillColor = {255, 0, 0}, fillPattern = FillPattern.Solid, extent = {{-62, 60}, {-50, 48}}), Line(origin = {1, 61}, points = {{-57, -7}, {57, -7}, {57, 7}}, color = {255, 0, 0}, pattern = LinePattern.Dash)}));
end MinMax;
