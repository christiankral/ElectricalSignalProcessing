within ElectricalSignalProcessing.Blocks.Continuous;
block Overshoot "Output is set equal to input when input overshoots threshold"
  extends Modelica.Blocks.Interfaces.SISO;
  parameter Real threshold = 0 "Threshold";
algorithm
  when u >= threshold then
    y := time;
  end when;
  when (-u) < threshold then
    y := time;
  end when;
  annotation (Icon(graphics={
        Polygon(
          points={{-16,-70},{-12,-52},{-20,-52},{-16,-70}},
          lineColor={255,0,0},
          fillColor={255,0,0},
          fillPattern=FillPattern.Solid),
        Line(points={{-16,40},{-16,-70}}, color={255,0,0}),
        Line(
          points={{-80,-70},{-60,-8},{-20,40},{24,62},{80,70}},
          color={0,0,0},
          smooth=Smooth.Bezier),
        Line(points={{-80,40},{80,40}}, color={255,0,0}),
        Ellipse(
          extent={{-22,46},{-10,34}},
          lineColor={0,0,0},
          fillColor={255,0,0},
          fillPattern=FillPattern.Solid)}));
end Overshoot;
