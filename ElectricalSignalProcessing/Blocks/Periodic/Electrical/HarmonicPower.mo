within ElectricalSignalProcessing.Blocks.Periodic.Electrical;
model HarmonicPower
  "Block to determine one harmonic power component from instantaneous voltage and current"
  extends Modelica.Blocks.Icons.Block;

  parameter Modelica.Units.SI.Frequency f(start=50) "Base frequency";
  parameter Integer k(start = 1) "Order of harmonic";

  Modelica.Blocks.Interfaces.RealInput v(final unit = "V") "Instantaneous voltage" annotation (Placement(transformation(extent={{-140,40},{-100,80}})));
  Modelica.Blocks.Interfaces.RealInput i(final unit = "A") "Instantaneous current" annotation (Placement(transformation(extent={{-140,-80},{-100,-40}})));

  Modelica.Blocks.Interfaces.RealOutput P(final unit="W") "Harmonic active power"
    annotation (Placement(transformation(
        origin={110,60},
        extent={{10,10},{-10,-10}},
        rotation=180)));
  Modelica.Blocks.Interfaces.RealOutput Q(final unit="var")
    "Reactive harmonic power" annotation (Placement(transformation(
        origin={110,20},
        extent={{10,10},{-10,-10}},
        rotation=180)));
  Modelica.Blocks.Interfaces.RealOutput S(final unit="V.A") "Harmonic apparent power" annotation (Placement(transformation(
        origin={110,-20},
        extent={{10,10},{-10,-10}},
        rotation=180)));
  Modelica.Blocks.Interfaces.RealOutput pf "Harmonic power factor" annotation (Placement(transformation(extent={{100,-70},{120,-50}})));

  Modelica.Blocks.Math.Harmonic harmonicCurrent(
    final f=f,
    final k=k,
    final x0Cos=0,
    final x0Sin=0) annotation (Placement(transformation(extent={{-80,-70},{-60,-50}})));
  Modelica.Blocks.Math.Harmonic harmonicVoltage(
    final f=f,
    final k=k,
    final x0Cos=0,
    final x0Sin=0) annotation (Placement(transformation(extent={{-80,50},{-60,70}})));
  Modelica.ComplexBlocks.ComplexMath.PolarToComplex toComplexCurrent annotation (Placement(transformation(extent={{-40,-70},{-20,-50}})));
  Modelica.ComplexBlocks.ComplexMath.PolarToComplex toComplexVoltage annotation (Placement(transformation(extent={{-40,50},{-20,70}})));
  Modelica.ComplexBlocks.ComplexMath.Product product(final useConjugateInput1=true, final useConjugateInput2=false) annotation (Placement(transformation(extent={{0,-10},{20,10}})));
  Modelica.ComplexBlocks.ComplexMath.ComplexToReal complexToReal(final useConjugateInput=false) annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=180,
        origin={50,30})));
  Modelica.ComplexBlocks.ComplexMath.ComplexToPolar complexToPolar(final useConjugateInput=false) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={50,-30})));
  Modelica.Blocks.Math.Cos cosine annotation (Placement(transformation(extent={{40,-80},{60,-60}})));
equation
  connect(toComplexVoltage.y, product.u1) annotation (Line(points={{-19,60},{-10,60},{-10,6},{-2,6}},    color={85,170,255}));
  connect(toComplexCurrent.y, product.u2) annotation (Line(points={{-19,-60},{-10,-60},{-10,-6},{-2,-6}},color={85,170,255}));
  connect(complexToReal.re, P) annotation (Line(points={{62,36},{80,36},{80,60},{110,60}},       color={0,0,127}));
  connect(complexToReal.im, Q) annotation (Line(points={{62,24},{80,24},{80,20},{110,20}},              color={0,0,127}));
  connect(complexToPolar.len, S) annotation (Line(points={{62,-24},{80,-24},{80,-20},{110,-20}}, color={0,0,127}));
  connect(harmonicVoltage.y_rms, toComplexVoltage.len) annotation (Line(points={{-59,66},{-42,66}}, color={0,0,127}));
  connect(harmonicVoltage.y_arg, toComplexVoltage.phi) annotation (Line(points={{-59,54},{-42,54}}, color={0,0,127}));
  connect(harmonicCurrent.y_rms, toComplexCurrent.len) annotation (Line(points={{-59,-54},{-42,-54}}, color={0,0,127}));
  connect(harmonicCurrent.y_arg, toComplexCurrent.phi) annotation (Line(points={{-59,-66},{-42,-66}}, color={0,0,127}));
  connect(v, harmonicVoltage.u) annotation (Line(points={{-120,60},{-82,60}}, color={0,0,127}));
  connect(i, harmonicCurrent.u) annotation (Line(points={{-120,-60},{-82,-60}}, color={0,0,127}));
  connect(product.y, complexToReal.u) annotation (Line(points={{21,0},{30,0},{30,30},{38,30}}, color={85,170,255}));
  connect(product.y, complexToPolar.u) annotation (Line(points={{21,0},{30,0},{30,-30},{38,-30}}, color={85,170,255}));
  connect(complexToPolar.phi, cosine.u) annotation (Line(points={{62,-36},{80,-36},{80,-50},{20,-50},{20,-70},{38,-70}}, color={0,0,127}));
  connect(cosine.y, pf) annotation (Line(points={{61,-70},{80,-70},{80,-60},{110,-60}}, color={0,0,127}));
  annotation (
    Icon(coordinateSystem(preserveAspectRatio=true, extent={{-100,-100},{100,100}}), graphics={
        Text(
          extent={{60,0},{100,-40}},
            textColor={64,64,64},
          textString="S"),
        Text(
          extent={{60,80},{100,40}},
            textColor={64,64,64},
          textString="P"),
        Text(
          extent={{60,40},{100,0}},
            textColor={64,64,64},
          textString="Q"),
        Rectangle(extent={{-76,80},{44,-80}}, lineColor={0,0,255}),
        Text(
          extent={{-56,70},{24,10}},
          textString="H%k"),
        Text(
          extent={{-56,-10},{24,-70}},
          textString="f=%f",
          lineColor={0,0,0}),
        Text(
          extent={{40,-40},{100,-80}},
            textColor={64,64,64},
          textString="pf")}),
    Documentation(info="<html>
<p>This signal processing block calculates the harmonic active, reactive and apparent power including the power factor with respect 
to the harmonic component number <code>k</code> and the base frequency <code>f</code>.</p>

<p>Due the harmonic analysis performed, this block provides the harmonic components after each full period determined by <code>1 / f</code>.
So there is a delay of one period for creating the output signals.</p>
</html>", revisions="<html>
<ul>
<li><em>20170306</em> first implementation by Anton Haumer</li>
</ul>
</html>"));
end HarmonicPower;
