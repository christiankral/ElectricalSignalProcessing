within ElectricalSignalProcessing.Blocks.Periodic;
block Mean "Calculate mean over period 1/f"
  extends Modelica.Blocks.Interfaces.SISO;
  import Modelica.Constants.small;
  import Modelica.Constants.inf;
  parameter Boolean use_fInput=false "Use f from input, else constant";
  parameter Modelica.Units.SI.Frequency fConst(start=50) "Base frequency"
    annotation (Dialog(enable=not use_fInput));
  parameter Real x0=0 "Start value of integrator state";
  parameter Boolean yGreaterOrEqualZero=false
    "=true, if output y is guaranteed to be >= 0 for the exact solution"
    annotation (Evaluate=true, Dialog(tab="Advanced"));
  Modelica.Blocks.Interfaces.RealInput fInput(unit="Hz")  if use_fInput "Variable frequency input"
    annotation (Placement(transformation(
        extent={{-20,-20},{20,20}},
        rotation=90,
        origin={0,-120})));
protected
  discrete Modelica.Units.SI.Time t0 "Sample time instants";
  Real x "Integrator state";
  Modelica.Blocks.Sources.Constant const(k=fConst) if not use_fInput
    annotation (Placement(transformation(extent={{-40,-90},{-20,-70}})));
  Modelica.Blocks.Interfaces.RealInput f "Connector of Real input signal"
    annotation (Placement(transformation(
        extent={{-4,-4},{4,4}},
        rotation=90,
        origin={0,-80})));
initial equation
  t0 = time;
  x = x0;
  y = 0;
equation
  der(x) = u;
  when time>=pre(t0) + (if noEvent(f<=small) then inf else 1/f) then
    y = if not yGreaterOrEqualZero then f*pre(x) else max(0.0, f*pre(x));
    reinit(x, 0);
    t0=time;
  end when;
  connect(fInput, f)
    annotation (Line(points={{0,-120},{0,-80}}, color={0,0,127}));
  connect(const.y, f)
    annotation (Line(points={{-19,-80},{0,-80}}, color={0,0,127}));
  annotation (Documentation(info="<html>
<p>
This block calculates the mean of the input signal u over the given period 1/f:
</p>
<pre>
1 T
- &int; u(t) dt
T 0
</pre>
<p>
Note: The output is updated after each period defined by 1/f.
</p>

<p>
If parameter <strong>yGreaterOrEqualZero</strong> in the Advanced tab is <strong>true</strong> (default = <strong>false</strong>),
then the modeller provides the information that the mean of the input signal is guaranteed
to be &ge; 0 for the exact solution. However, due to inaccuracies in the numerical integration scheme,
the output might be slightly negative. If this parameter is set to true, then the output is
explicitly set to 0.0, if the mean value results in a negative value.
</p>
</html>"),
         Icon(graphics={Text(
          extent={{-80,60},{80,20}},
          textString="mean")}));
end Mean;
