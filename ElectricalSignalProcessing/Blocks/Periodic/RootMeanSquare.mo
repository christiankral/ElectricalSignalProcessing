within ElectricalSignalProcessing.Blocks.Periodic;
block RootMeanSquare "Calculate root mean square over period 1/f"
  extends Modelica.Blocks.Interfaces.SISO;
  parameter Boolean use_fInput=false "Use f from input, else constant";
  parameter Modelica.Units.SI.Frequency fConst(start=50) "Base frequency"
    annotation (Dialog(enable=not use_fInput));
  parameter Real x0=0 "Start value of integrator state";
  Modelica.Blocks.Math.MultiProduct product(nu=2)
    annotation (Placement(transformation(extent={{-50,-10},{-30,10}})));
  Periodic.Mean mean(
    final use_fInput=use_fInput,
    final fConst=fConst,
    final yGreaterOrEqualZero=true,
    final x0=x0)
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  Modelica.Blocks.Math.Sqrt sqrt1
    annotation (Placement(transformation(extent={{30,-10},{50,10}})));
  Modelica.Blocks.Interfaces.RealInput fInput(unit="Hz") if use_fInput "Variable frequency input"
    annotation (Placement(transformation(
        extent={{-20,-20},{20,20}},
        rotation=90,
        origin={0,-120})));
equation

  connect(product.y, mean.u) annotation (Line(
      points={{-28.3,0},{-12,0}},color={0,0,127}));
  connect(mean.y, sqrt1.u) annotation (Line(
      points={{11,0},{28,0}}, color={0,0,127}));
  connect(sqrt1.y, y) annotation (Line(
      points={{51,0},{110,0}}, color={0,0,127}));
  connect(u, product.u[1]) annotation (Line(
      points={{-120,0},{-70,0},{-70,3.5},{-50,3.5}}, color={0,0,127}));
  connect(u, product.u[2]) annotation (Line(
      points={{-120,0},{-70,0},{-70,-3.5},{-50,-3.5}}, color={0,0,127}));
  connect(fInput, mean.fInput)
    annotation (Line(points={{0,-120},{0,-12}}, color={0,0,127}));
  annotation (Documentation(info="<html>
<p>
This block calculates the root mean square of the input signal u over the given period 1/f, using the
<a href=\"modelica://Modelica.Blocks.Math.Mean\">mean block</a>.
</p>
<p>
Note: The output is updated after each period defined by 1/f.
</p>
</html>"),
         Icon(graphics={Text(
          extent={{-80,60},{80,20}},
          textString="RMS")}));
end RootMeanSquare;
