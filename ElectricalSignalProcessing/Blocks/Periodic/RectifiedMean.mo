within ElectricalSignalProcessing.Blocks.Periodic;
block RectifiedMean "Calculate rectified mean over period 1/f"
  extends Modelica.Blocks.Interfaces.SISO;
  parameter Boolean use_fInput=false "Use f from input, else constant";
  parameter Modelica.Units.SI.Frequency fConst(start=50) "Base frequency"
    annotation (Dialog(enable=not use_fInput));
  parameter Real x0=0 "Start value of integrator state";
  Periodic.Mean mean(
    use_fInput=use_fInput,
    fConst=fConst,
    final x0=x0)
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  Modelica.Blocks.Math.Abs abs1
    annotation (Placement(transformation(extent={{-60,-10},{-40,10}})));
  Modelica.Blocks.Interfaces.RealInput fInput(unit="Hz") if use_fInput "Variable frequency input"
    annotation (Placement(transformation(
        extent={{-20,-20},{20,20}},
        rotation=90,
        origin={0,-120})));
equation
  connect(u, abs1.u) annotation (Line(
      points={{-120,0},{-62,0}}, color={0,0,127}));
  connect(abs1.y, mean.u) annotation (Line(
      points={{-39,0},{-12,0}},color={0,0,127}));
  connect(mean.y, y) annotation (Line(
      points={{11,0},{110,0}}, color={0,0,127}));
  connect(mean.fInput, fInput)
    annotation (Line(points={{0,-12},{0,-120}}, color={0,0,127}));
  annotation (Documentation(info="<html>
<p>
This block calculates the rectified mean of the input signal u over the given period 1/f, using the
<a href=\"modelica://Modelica.Blocks.Math.Mean\">mean block</a>.
</p>
<p>
Note: The output is updated after each period defined by 1/f.
</p>
</html>"),
         Icon(graphics={Text(
          extent={{-80,60},{80,20}},
          textString="RM")}));
end RectifiedMean;
