within ElectricalSignalProcessing.Examples;
model TestFrequencyDetection
  "Test signal processing blocks with variable frequency"
  extends Modelica.Icons.Example;
  import Modelica.Constants.pi;
  Modelica.Electrical.Machines.Utilities.VfController vfController(
    m=3,
    VNominal=1,
    fNominal=50)
    annotation (Placement(transformation(extent={{-30,-10},{-10,10}})));
  Modelica.Blocks.Sources.Ramp ramp(
    height=50,
    duration=1,
    offset=0,
    startTime=0.25)
    annotation (Placement(transformation(extent={{-70,-10},{-50,10}})));
  Blocks.Continuous.DetectAngleSpeed detectAngleSpeed
    annotation (Placement(transformation(extent={{10,-10},{30,10}})));
  Modelica.Blocks.Math.Gain gain(k=2*pi)
    annotation (Placement(transformation(extent={{-30,-40},{-10,-20}})));
equation
  connect(ramp.y, vfController.u)
    annotation (Line(points={{-49,0},{-32,0}}, color={0,0,127}));
  connect(gain.y, detectAngleSpeed.wEstimation)
    annotation (Line(points={{-9,-30},{20,-30},{20,-12}},
                                                       color={0,0,127}));
  connect(vfController.y, detectAngleSpeed.u)
    annotation (Line(points={{-9,0},{8,0}}, color={0,0,127}));
  connect(ramp.y, gain.u) annotation (Line(points={{-49,0},{-40,0},{-40,-30},{
          -32,-30}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    experiment(StopTime=2, Interval=0.0005));
end TestFrequencyDetection;
