within ElectricalSignalProcessing.Examples;
model TestMinMax "Testing of minmum and maximum blocks"
  extends Modelica.Icons.Example;
  Blocks.Continuous.MinMax continuousMinMax1(stopTime=0.3)
    annotation (Placement(transformation(extent={{20,10},{40,30}})));
  Blocks.Continuous.SubsequentMax subsequentMax
    annotation (Placement(transformation(extent={{20,50},{40,70}})));
  Blocks.Continuous.SubsequentMin subsequentMin
    annotation (Placement(transformation(extent={{20,-80},{40,-60}})));
  Blocks.Sampled.MinMax sampledMinMax1(samplePeriod=1E-4)
    annotation (Placement(transformation(extent={{60,10},{80,30}})));
  Blocks.Continuous.MinMax continuousMinMax2(startTime=0.5, stopTime=0.8)
    annotation (Placement(transformation(extent={{20,-30},{40,-10}})));
  Blocks.Sampled.MinMax sampledMinMax2(samplePeriod=1E-4, startTime=0.45)
    annotation (Placement(transformation(extent={{60,-30},{80,-10}})));
  Modelica.Blocks.Sources.ExpSine expSine(
    
    damping=2,f=10,
    phase=1.5707963267949)
    annotation (Placement(transformation(extent={{-80,10},{-60,30}})));
  Modelica.Blocks.Sources.Step step(startTime= 0.55)
    annotation (Placement(transformation(extent={{-80,-40},{-60,-20}})));
  Modelica.Blocks.Math.Add add
    annotation (Placement(transformation(extent={{-40,-10},{-20,10}})));
  ElectricalSignalProcessing.Blocks.Continuous.MinMaxFirstOrder minMaxFirstOrder annotation(
    Placement(visible = true, transformation(origin = {30, 90}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(sampledMinMax1.u, subsequentMax.u) annotation(
    Line(points = {{58, 20}, {50, 20}, {50, 40}, {0, 40}, {0, 60}, {18, 60}}, color = {0, 0, 127}));
  connect(continuousMinMax2.u, subsequentMin.u) annotation(
    Line(points = {{18, -20}, {0, -20}, {0, -70}, {18, -70}}, color = {0, 0, 127}));
  connect(sampledMinMax2.u, subsequentMin.u) annotation(
    Line(points = {{58, -20}, {50, -20}, {50, -40}, {0, -40}, {0, -70}, {18, -70}}, color = {0, 0, 127}));
  connect(continuousMinMax1.u, subsequentMax.u) annotation(
    Line(points = {{18, 20}, {0, 20}, {0, 60}, {18, 60}}, color = {0, 0, 127}));
  connect(continuousMinMax2.u, continuousMinMax1.u) annotation(
    Line(points = {{18, -20}, {0, -20}, {0, 20}, {18, 20}}, color = {0, 0, 127}));
  connect(add.y, continuousMinMax1.u) annotation(
    Line(points = {{-19, 0}, {0, 0}, {0, 20}, {18, 20}}, color = {0, 0, 127}));
  connect(expSine.y, add.u1) annotation(
    Line(points = {{-59, 20}, {-50, 20}, {-50, 6}, {-42, 6}}, color = {0, 0, 127}));
  connect(step.y, add.u2) annotation(
    Line(points = {{-59, -30}, {-50, -30}, {-50, -6}, {-42, -6}}, color = {0, 0, 127}));
  connect(add.y, minMaxFirstOrder.u) annotation(
    Line(points = {{-18, 0}, {0, 0}, {0, 90}, {18, 90}}, color = {0, 0, 127}));
  annotation (experiment(Tolerance=1e-06));
end TestMinMax;