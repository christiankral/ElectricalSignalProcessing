within ElectricalSignalProcessing.Examples;
model TestVariableFrequency "Test signal processing blocks with variable frequency"
  extends Modelica.Icons.Example;
  import Modelica.Constants.pi;
  Modelica.Electrical.Machines.Utilities.VfController vfController(
    m=1,
    VNominal=1,
    fNominal=50)
    annotation (Placement(transformation(extent={{-30,-10},{-10,10}})));
  Modelica.Blocks.Sources.Ramp ramp(
    height=50,
    duration=1,
    offset=0,
    startTime=0.25)
    annotation (Placement(transformation(extent={{-70,-10},{-50,10}})));
  Blocks.Periodic.Mean mean(use_fInput=true, fConst=50)
    annotation (Placement(transformation(extent={{10,10},{30,30}})));
  Blocks.Periodic.RectifiedMean rectifiedMean(use_fInput=true, fConst=50)
    annotation (Placement(transformation(extent={{30,-10},{50,10}})));
  Blocks.Periodic.RootMeanSquare rootMeanSquare(use_fInput=true, fConst=50)
    annotation (Placement(transformation(extent={{50,-30},{70,-10}})));
equation
  connect(ramp.y, vfController.u)
    annotation (Line(points={{-49,0},{-32,0}}, color={0,0,127}));
  connect(vfController.y[1], mean.u)
    annotation (Line(points={{-9,0},{0,0},{0,20},{8,20}}, color={0,0,127}));
  connect(vfController.y[1], rectifiedMean.u)
    annotation (Line(points={{-9,0},{28,0}}, color={0,0,127}));
  connect(vfController.y[1], rootMeanSquare.u)
    annotation (Line(points={{-9,0},{0,0},{0,-20},{48,-20}}, color={0,0,127}));
  connect(ramp.y, mean.fInput) annotation (Line(points={{-49,0},{-40,0},{-40,-40},
          {20,-40},{20,8}}, color={0,0,127}));
  connect(ramp.y, rectifiedMean.fInput) annotation (Line(points={{-49,0},{-40,0},
          {-40,-40},{40,-40},{40,-12}}, color={0,0,127}));
  connect(ramp.y, rootMeanSquare.fInput) annotation (Line(points={{-49,0},{-40,0},
          {-40,-40},{60,-40},{60,-32}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    experiment(StopTime=2, Interval=0.001));
end TestVariableFrequency;
