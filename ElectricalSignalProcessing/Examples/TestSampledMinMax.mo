within ElectricalSignalProcessing.Examples;
model TestSampledMinMax
  "Test detection of sampled minimum and maximum"
  extends Modelica.Icons.Example;
  Blocks.Sampled.MinMax signalExtrema1(samplePeriod=1e-2)
    annotation (Placement(transformation(extent={{40,10},{60,30}})));
  Blocks.Sampled.MinMax signalExtrema2(samplePeriod=1E-4)
    annotation (Placement(transformation(extent={{40,-30},{60,-10}})));
  Modelica.Blocks.Sources.ExpSine expSine(
    amplitude=1,
    f=19,
    damping=-2)
    annotation (Placement(transformation(extent={{-50,20},{-30,40}})));
  Modelica.Blocks.Sources.Step step(startTime=0.5)
    annotation (Placement(transformation(extent={{-50,-40},{-30,-20}})));
  Modelica.Blocks.Math.Add add
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
equation
  connect(expSine.y,add. u1) annotation (Line(points={{-29,30},{-20,30},{-20,6},
          {-12,6}}, color={0,0,127}));
  connect(step.y,add. u2) annotation (Line(points={{-29,-30},{-20,-30},{-20,-6},
          {-12,-6}}, color={0,0,127}));
  connect(add.y, signalExtrema1.u)
    annotation (Line(points={{11,0},{20,0},{20,20},{38,20}}, color={0,0,127}));
  connect(add.y, signalExtrema2.u) annotation (Line(points={{11,0},{20,0},{20,
          -20},{38,-20}}, color={0,0,127}));
  annotation (experiment(
      StopTime=1.5,
      Interval=1e-05,
      Tolerance=1e-06), Documentation(info="<html>
<p>
This example uses a sinusoidal signal with amplitude varying sinusoidally in the range of [1,5] with a frequency of 63 Hz,
and frequency varying according to a cosine function in the range of [10, 100] Hz with a frqeuncy of 77 Hz.
</p>
<p>
Note that signalExtrema1 doesn't find the extrema exactly since sampling frequency 100 Hz is too small compared to maximum frequency of the input signal,
whereas signalExtrema2 catches the extrema rather good due to the fact that sampling frequency 10 kHz is high enough.
</p>
</html>"));
end TestSampledMinMax;
