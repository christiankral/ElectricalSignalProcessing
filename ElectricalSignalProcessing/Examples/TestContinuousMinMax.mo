within ElectricalSignalProcessing.Examples;
model TestContinuousMinMax "Test ContinuousMinMax model"
  extends Modelica.Icons.Example;
  Modelica.Blocks.Sources.ExpSine expSine(
    amplitude=1,
    f=10,
    damping=-2)
    annotation (Placement(transformation(extent={{-60,20},{-40,40}})));
  Modelica.Blocks.Sources.Step step(startTime=0.5)
    annotation (Placement(transformation(extent={{-60,-40},{-40,-20}})));
  Modelica.Blocks.Math.Add add
    annotation (Placement(transformation(extent={{-20,-10},{0,10}})));
  Blocks.Continuous.MinMax continuousMaxMin(timeReset=0.5)
    annotation (Placement(transformation(extent={{20,-10},{40,10}})));
equation
  connect(expSine.y, add.u1) annotation (Line(points={{-39,30},{-30,30},{-30,6},
          {-22,6}}, color={0,0,127}));
  connect(add.y, continuousMaxMin.u)
    annotation (Line(points={{1,0},{18,0}}, color={0,0,127}));
  connect(step.y, add.u2) annotation (Line(points={{-39,-30},{-30,-30},{-30,-6},
          {-22,-6}}, color={0,0,127}));
  annotation (
    Icon(coordinateSystem(preserveAspectRatio=false)),
    Diagram(coordinateSystem(preserveAspectRatio=false)));
end TestContinuousMinMax;
