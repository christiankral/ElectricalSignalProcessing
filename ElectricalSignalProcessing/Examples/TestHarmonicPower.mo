within ElectricalSignalProcessing.Examples;
model TestHarmonicPower "Test example of harmonic power"
  extends Modelica.Icons.Example;

  parameter Modelica.Units.SI.Frequency f=50 "Frequency";
  parameter Integer k = 1 "Harmonic order";
  parameter Modelica.Units.SI.Voltage Vrms=100 "RMS supply voltage";
  parameter Modelica.Units.SI.Resistance R=1 "Load resistance";
  parameter Modelica.Units.SI.Inductance L=1E-3 "Load inductance";
  final parameter Modelica.Units.SI.AngularVelocity w=2*Modelica.Constants.pi*f
    "Angular frequency";
  final parameter Modelica.Units.SI.Current Irms=Vrms/sqrt(R^2 + (w*L)^2)
    "RMS current";
  final parameter Modelica.Units.SI.Power P=R*Irms^2 "Active power";
  final parameter Modelica.Units.SI.ReactivePower Q=w*L*Irms^2 "Reactive power";
  final parameter Modelica.Units.SI.ApparentPower S=sqrt(P^2 + Q^2)
    "Apparent power";
  final parameter Real pf = P / S "Power factor";

  Modelica.Electrical.Analog.Basic.Resistor resistor(R = R) annotation (Placement(visible=true, transformation(
        origin={60,20},
        extent={{-10,-10},{10,10}},
        rotation=270)));
  Modelica.Electrical.Analog.Basic.Inductor inductor(L = L)  annotation (
      Placement(visible=true, transformation(
        origin={60,-22},
        extent={{-10,-10},{10,10}},
        rotation=270)));
  Modelica.Electrical.Analog.Basic.Ground ground annotation (Placement(
        visible=true, transformation(extent={{-30,-70},{-10,-50}},rotation=0)));
  Modelica.Electrical.Analog.Sources.SineVoltage sineVoltage(f=f, V=Vrms*sqrt(2))
    annotation (Placement(visible=true, transformation(
        origin={-60,0},
        extent={{-10,10},{10,-10}},
        rotation=270)));
  Blocks.Periodic.Electrical.HarmonicPower harmonicPower(f=f, k=k)
    annotation (Placement(transformation(extent={{0,-10},{20,10}})));
  Modelica.Electrical.Analog.Sensors.MultiSensor multiSensor annotation (Placement(transformation(extent={{-40,30},{-20,50}})));
equation
  connect(resistor.n, inductor.p) annotation (Line(points={{60,10},{60,-12}},
                                                                            color={0,0,255}));
  connect(sineVoltage.n, inductor.n) annotation (Line(points={{-60,-10},{-60,-40},{60,-40},{60,-32}},
                                        color={0,0,255}));
  connect(ground.p, inductor.n) annotation (Line(points={{-20,-50},{-20,-40},{60,-40},{60,-32}},
                          color={0,0,255}));
  connect(sineVoltage.p, multiSensor.pc) annotation (Line(points={{-60,10},{-60,40},{-40,40}}, color={0,0,255}));
  connect(multiSensor.nc, resistor.p) annotation (Line(points={{-20,40},{60,40},{60,30}}, color={0,0,255}));
  connect(multiSensor.nv, inductor.n) annotation (Line(points={{-30,30},{-30,-40},{60,-40},{60,-32}}, color={0,0,255}));
  connect(multiSensor.pv, multiSensor.pc) annotation (Line(points={{-30,50},{-40,50},{-40,40}}, color={0,0,255}));
  connect(multiSensor.i, harmonicPower.i) annotation (Line(points={{-36,29},{-36,-6},{-2,-6}},          color={0,0,127}));
  connect(multiSensor.v, harmonicPower.v) annotation (Line(points={{-24,29},{-24,6},{-2,6}},   color={0,0,127}));
  annotation (
    experiment(StopTime=0.1, Tolerance=1e-06),
    Documentation(info="<html>
<p>Test the component <a href=\"modelica://ElectricalSignalProcessing.Blocks.HarmonicPower\">HarmonicPower</a> by comparing the following singals:</p>

<table border=\"1\" cellspacing=\"0\" cellpadding=\"2\">
  <tr>
    <th>Quasi static</th>
    <th>Block signal</th>
    <th>Comment</th>
  </tr>
  <tr>
    <td><code>P</code></td>
    <td><code>harmoicPower.P</code></td>
    <td>Active power</td>
  </tr>
  <tr>
    <td><code>Q</code></td>
    <td><code>harmoicPower.Q</code></td>
    <td>Reactive power</td>
  </tr>
  <tr>
    <td><code>S</code></td>
    <td><code>harmoicPower.S</code></td>
    <td>Apparent power</td>
  </tr>
  <tr>
    <td><code>pf</code></td>
    <td><code>harmoicPower.pf</code></td>
    <td>Power facotr</td>
  </tr>
</table>

</html>",revisions = "<html><head></head><body>Non va newInst (non va avg)</body></html>"),
    Diagram(coordinateSystem(
        extent={{-100,-100},{100,100}},
        preserveAspectRatio=false)),
    Icon(coordinateSystem(
        preserveAspectRatio=false)));
end TestHarmonicPower;
